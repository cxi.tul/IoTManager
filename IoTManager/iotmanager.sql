--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 10.1

-- Started on 2018-02-09 16:41:39

SET statement_timeout = 0;
SET lock_timeout = 0;
--SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12429)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2370 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 227 (class 1255 OID 16789)
-- Name: process_audit(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION process_audit() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
  DECLARE
  arg TEXT;
    BEGIN
        --
        -- Create a row in emp_audit to reflect the operation performed on emp,
        -- make use of the special variable TG_OP to work out the operation.
        --
        FOREACH arg IN ARRAY TG_ARGV LOOP
		IF (TG_OP = 'DELETE') THEN
		    EXECUTE format('INSERT INTO %s SELECT ''D'', now(), $1.*', arg)
		    USING OLD;
		    RETURN OLD;
		ELSIF (TG_OP = 'UPDATE') THEN
		    EXECUTE format('INSERT INTO %s SELECT ''U'', now(), $1.*', arg)
		    USING NEW;
		    RETURN NEW;
		ELSIF (TG_OP = 'INSERT') THEN
		    EXECUTE format('INSERT INTO %s SELECT ''I'', now(), $1.*', arg)
		    USING NEW;
		    RETURN NEW;
		END IF;
		RETURN NULL;  -- result is ignored since this is an AFTER trigger
        END LOOP;
    END;
$_$;


--
-- TOC entry 214 (class 1255 OID 16737)
-- Name: process_functions_audit(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION process_functions_audit() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        --
        -- Create a row in emp_audit to reflect the operation performed on emp,
        -- make use of the special variable TG_OP to work out the operation.
        --
        IF (TG_OP = 'DELETE') THEN
            INSERT INTO functions_audit SELECT 'D', now(), OLD.*;
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO functions_audit SELECT 'U', now(), NEW.*;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO functions_audit SELECT 'I', now(), NEW.*;
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$$;


SET default_with_oids = false;

--
-- TOC entry 191 (class 1259 OID 16469)
-- Name: devices; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE devices (
    device_id integer NOT NULL,
    ip cidr,
    description character varying(255),
    group_id integer,
    status_id integer,
    online boolean,
    ts_last_online timestamp without time zone,
    app_version character varying(100),
    firmware_version character varying(100),
    ts_last_update timestamp without time zone,
    uuid uuid NOT NULL,
    testing boolean DEFAULT false NOT NULL,
    modified_by character varying(250)
);


--
-- TOC entry 190 (class 1259 OID 16467)
-- Name: device_device_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE device_device_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2371 (class 0 OID 0)
-- Dependencies: 190
-- Name: device_device_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE device_device_id_seq OWNED BY devices.device_id;


--
-- TOC entry 188 (class 1259 OID 16457)
-- Name: device_groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE device_groups (
    group_id integer NOT NULL,
    name character varying(100) NOT NULL,
    modified_by character varying(250)
);


--
-- TOC entry 205 (class 1259 OID 16748)
-- Name: device_groups_audit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE device_groups_audit (
    operation character(1) NOT NULL,
    stamp timestamp without time zone NOT NULL,
    group_id integer NOT NULL,
    name character varying(100) NOT NULL,
    modified_by character varying(250)
);


--
-- TOC entry 189 (class 1259 OID 16462)
-- Name: device_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE device_status (
    status_id integer NOT NULL,
    name character varying(100),
    modified_by character varying(250)
);


--
-- TOC entry 213 (class 1259 OID 16784)
-- Name: device_status_audit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE device_status_audit (
    operation character(1) NOT NULL,
    stamp timestamp without time zone NOT NULL,
    status_id integer NOT NULL,
    name character varying(100),
    modified_by character varying(250)
);


--
-- TOC entry 206 (class 1259 OID 16754)
-- Name: devices_audit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE devices_audit (
    operation character(1) NOT NULL,
    stamp timestamp without time zone NOT NULL,
    device_id integer NOT NULL,
    ip cidr,
    description character varying(255),
    group_id integer,
    status_id integer,
    online boolean,
    ts_last_online timestamp without time zone,
    app_version character varying(100),
    firmware_version character varying(100),
    ts_last_update timestamp without time zone,
    uuid uuid NOT NULL,
    testing boolean NOT NULL,
    modified_by character varying(250)
);


--
-- TOC entry 193 (class 1259 OID 16493)
-- Name: functions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE functions (
    function_id integer NOT NULL,
    description character varying(255),
    local boolean DEFAULT false NOT NULL,
    parameters text,
    inputs text,
    outputs text,
    modified_by character varying(250),
    name character varying(100),
    format character varying(10)
);


--
-- TOC entry 192 (class 1259 OID 16491)
-- Name: function_function_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE function_function_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2372 (class 0 OID 0)
-- Dependencies: 192
-- Name: function_function_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE function_function_id_seq OWNED BY functions.function_id;


--
-- TOC entry 202 (class 1259 OID 16730)
-- Name: functions_audit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE functions_audit (
    operation character(1) NOT NULL,
    stamp timestamp without time zone NOT NULL,
    function_id integer,
    description character varying(255),
    local boolean DEFAULT false NOT NULL,
    parameters text,
    inputs text,
    outputs text,
    modified_by character varying(250),
    name character varying(100),
    format character varying(10)
);


--
-- TOC entry 187 (class 1259 OID 16452)
-- Name: rights; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE rights (
    right_id integer NOT NULL,
    name character varying(100) NOT NULL,
    modified_by character varying(250)
);


--
-- TOC entry 204 (class 1259 OID 16745)
-- Name: rights_audit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE rights_audit (
    operation character(1) NOT NULL,
    stamp timestamp without time zone NOT NULL,
    right_id integer NOT NULL,
    name character varying(100) NOT NULL,
    modified_by character varying(250)
);


--
-- TOC entry 201 (class 1259 OID 16685)
-- Name: task_host; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE task_host (
    task_id integer NOT NULL,
    host_id integer NOT NULL,
    device_id integer,
    function_id integer NOT NULL,
    version_id integer,
    modified_by character varying(250)
);


--
-- TOC entry 212 (class 1259 OID 16781)
-- Name: task_host_audit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE task_host_audit (
    operation character(1) NOT NULL,
    stamp timestamp without time zone NOT NULL,
    task_id integer NOT NULL,
    host_id integer NOT NULL,
    device_id integer,
    function_id integer NOT NULL,
    version_id integer,
    modified_by character varying(250)
);


--
-- TOC entry 195 (class 1259 OID 16499)
-- Name: tasks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tasks (
    task_id integer NOT NULL,
    description character varying(255),
    active boolean DEFAULT false NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    hosts text,
    version integer DEFAULT 0 NOT NULL,
    testing boolean DEFAULT false NOT NULL,
    modified_by character varying(250),
    name character varying(100)
);


--
-- TOC entry 194 (class 1259 OID 16497)
-- Name: task_task_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE task_task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2373 (class 0 OID 0)
-- Dependencies: 194
-- Name: task_task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE task_task_id_seq OWNED BY tasks.task_id;


--
-- TOC entry 207 (class 1259 OID 16760)
-- Name: tasks_audit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tasks_audit (
    operation character(1) NOT NULL,
    stamp timestamp without time zone NOT NULL,
    task_id integer NOT NULL,
    description character varying(255),
    active boolean NOT NULL,
    deleted boolean NOT NULL,
    hosts text,
    version integer NOT NULL,
    testing boolean NOT NULL,
    modified_by character varying(250),
    name character varying(100)
);


--
-- TOC entry 197 (class 1259 OID 16506)
-- Name: user_group; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_group (
    user_id integer NOT NULL,
    group_id integer NOT NULL,
    modified_by character varying(250)
);


--
-- TOC entry 209 (class 1259 OID 16769)
-- Name: user_group_audit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_group_audit (
    operation character(1) NOT NULL,
    stamp timestamp without time zone NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL,
    modified_by character varying(250)
);


--
-- TOC entry 196 (class 1259 OID 16503)
-- Name: user_right; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_right (
    user_id integer NOT NULL,
    rights_id integer NOT NULL,
    modified_by character varying(250)
);


--
-- TOC entry 208 (class 1259 OID 16766)
-- Name: user_right_audit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_right_audit (
    operation character(1) NOT NULL,
    stamp timestamp without time zone NOT NULL,
    user_id integer NOT NULL,
    rights_id integer NOT NULL,
    modified_by character varying(250)
);


--
-- TOC entry 186 (class 1259 OID 16443)
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    user_id integer NOT NULL,
    name character varying(250),
    mail character varying(250),
    pwd character varying(100),
    username character varying(100) NOT NULL,
    uuid uuid NOT NULL,
    modified_by character varying(250)
);


--
-- TOC entry 203 (class 1259 OID 16739)
-- Name: users_audit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users_audit (
    operation character(1) NOT NULL,
    stamp timestamp without time zone NOT NULL,
    user_id integer NOT NULL,
    name character varying(250),
    mail character varying(250),
    pwd character varying(100),
    username character varying(100) NOT NULL,
    uuid uuid NOT NULL,
    modified_by character varying(250)
);


--
-- TOC entry 185 (class 1259 OID 16441)
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2374 (class 0 OID 0)
-- Dependencies: 185
-- Name: users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_user_id_seq OWNED BY users.user_id;


--
-- TOC entry 200 (class 1259 OID 16550)
-- Name: version_state; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE version_state (
    version_state_id integer NOT NULL,
    name character varying(100) NOT NULL,
    modified_by character varying(250)
);


--
-- TOC entry 211 (class 1259 OID 16778)
-- Name: version_state_audit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE version_state_audit (
    operation character(1) NOT NULL,
    stamp timestamp without time zone NOT NULL,
    version_state_id integer NOT NULL,
    name character varying(100) NOT NULL,
    modified_by character varying(250)
);


--
-- TOC entry 199 (class 1259 OID 16539)
-- Name: versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE versions (
    version_id integer NOT NULL,
    function_id integer NOT NULL,
    number character varying(100) NOT NULL,
    description character varying(255),
    version_state_id integer NOT NULL,
    code bytea,
    modified_by character varying(250)
);


--
-- TOC entry 198 (class 1259 OID 16537)
-- Name: version_version_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE version_version_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2375 (class 0 OID 0)
-- Dependencies: 198
-- Name: version_version_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE version_version_id_seq OWNED BY versions.version_id;


--
-- TOC entry 210 (class 1259 OID 16772)
-- Name: versions_audit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE versions_audit (
    operation character(1) NOT NULL,
    stamp timestamp without time zone NOT NULL,
    version_id integer NOT NULL,
    function_id integer NOT NULL,
    number character varying(100) NOT NULL,
    description character varying(255),
    version_state_id integer NOT NULL,
    code bytea,
    modified_by character varying(250)
);


--
-- TOC entry 2154 (class 2604 OID 16472)
-- Name: devices device_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY devices ALTER COLUMN device_id SET DEFAULT nextval('device_device_id_seq'::regclass);


--
-- TOC entry 2156 (class 2604 OID 16496)
-- Name: functions function_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY functions ALTER COLUMN function_id SET DEFAULT nextval('function_function_id_seq'::regclass);


--
-- TOC entry 2158 (class 2604 OID 16502)
-- Name: tasks task_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tasks ALTER COLUMN task_id SET DEFAULT nextval('task_task_id_seq'::regclass);


--
-- TOC entry 2153 (class 2604 OID 16446)
-- Name: users user_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN user_id SET DEFAULT nextval('users_user_id_seq'::regclass);


--
-- TOC entry 2163 (class 2604 OID 16542)
-- Name: versions version_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY versions ALTER COLUMN version_id SET DEFAULT nextval('version_version_id_seq'::regclass);


--
-- TOC entry 2338 (class 0 OID 16457)
-- Dependencies: 188
-- Data for Name: device_groups; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO device_groups VALUES (1, 'group 1', NULL);
INSERT INTO device_groups VALUES (2, 'group 2', NULL);


--
-- TOC entry 2355 (class 0 OID 16748)
-- Dependencies: 205
-- Data for Name: device_groups_audit; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2339 (class 0 OID 16462)
-- Dependencies: 189
-- Data for Name: device_status; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2363 (class 0 OID 16784)
-- Dependencies: 213
-- Data for Name: device_status_audit; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2341 (class 0 OID 16469)
-- Dependencies: 191
-- Data for Name: devices; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO devices VALUES (4, NULL, 'treti', 2, NULL, NULL, NULL, NULL, NULL, NULL, 'aeed3280-47ff-4deb-beae-4cb5f9466341', false, NULL);
INSERT INTO devices VALUES (13, NULL, 'Nove zarizeni', 2, NULL, NULL, NULL, NULL, NULL, NULL, '5369ca1f-64a4-4ff8-bc2d-6ee18799c7bb', false, NULL);
INSERT INTO devices VALUES (12, NULL, 'Ovladač rolet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '831c87cf-9e2a-4a7c-b5c2-2cadb2e072e1', false, NULL);
INSERT INTO devices VALUES (14, NULL, 'Čtečka čárových kódů', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6c0ee7fa-eadd-4ba0-b51f-626023e34ee3', false, NULL);
INSERT INTO devices VALUES (26, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '903297e8-55b3-45ba-a9a2-db685433bed1', false, 'guest');
INSERT INTO devices VALUES (2, NULL, 'Anemometr na střeše CXI 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '439988ff-c33a-44ab-aba8-39951216edbf', false, 'guest');
INSERT INTO devices VALUES (15, NULL, 'Monitor na lince', 1, NULL, NULL, NULL, NULL, NULL, NULL, '6eb08e54-3d20-412a-94be-f55c4be05d0a', false, 'guest');


--
-- TOC entry 2356 (class 0 OID 16754)
-- Dependencies: 206
-- Data for Name: devices_audit; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO devices_audit VALUES ('I', '2017-12-19 20:39:28.977693', 17, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4a4c4a1d-f281-46eb-81b7-d56708f4a9be', false, 'guest');
INSERT INTO devices_audit VALUES ('U', '2017-12-19 20:39:47.648596', 17, NULL, 'Nove zarizeni', 1, NULL, NULL, NULL, NULL, NULL, NULL, '4a4c4a1d-f281-46eb-81b7-d56708f4a9be', false, 'guest');
INSERT INTO devices_audit VALUES ('U', '2017-12-19 20:40:10.272177', 15, NULL, 'Monitor na lince', 1, NULL, NULL, NULL, NULL, NULL, NULL, '6eb08e54-3d20-412a-94be-f55c4be05d0a', false, 'guest');
INSERT INTO devices_audit VALUES ('D', '2017-12-19 20:40:51.551735', 17, NULL, 'Nove zarizeni', 1, NULL, NULL, NULL, NULL, NULL, NULL, '4a4c4a1d-f281-46eb-81b7-d56708f4a9be', false, 'guest');
INSERT INTO devices_audit VALUES ('I', '2017-12-19 21:07:02.19772', 18, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '981d701f-0688-4abf-8393-32d42bf40922', false, 'guest');
INSERT INTO devices_audit VALUES ('U', '2017-12-19 21:07:07.972715', 18, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '981d701f-0688-4abf-8393-32d42bf40922', false, 'guest');
INSERT INTO devices_audit VALUES ('D', '2017-12-19 21:07:07.976516', 18, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '981d701f-0688-4abf-8393-32d42bf40922', false, 'guest');
INSERT INTO devices_audit VALUES ('I', '2017-12-20 06:41:23.478838', 19, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3d66d463-356e-4b8f-80e6-b6e5b18b083d', false, NULL);
INSERT INTO devices_audit VALUES ('U', '2017-12-20 06:42:08.308093', 19, NULL, 'Nove zarizeni 123', 1, NULL, NULL, NULL, NULL, NULL, NULL, '3d66d463-356e-4b8f-80e6-b6e5b18b083d', false, NULL);
INSERT INTO devices_audit VALUES ('D', '2017-12-20 06:42:15.112996', 19, NULL, 'Nove zarizeni 123', 1, NULL, NULL, NULL, NULL, NULL, NULL, '3d66d463-356e-4b8f-80e6-b6e5b18b083d', false, NULL);
INSERT INTO devices_audit VALUES ('I', '2017-12-21 15:33:54.680802', 20, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '272a7292-9b75-405c-9886-e892dc8aa2f6', false, NULL);
INSERT INTO devices_audit VALUES ('I', '2017-12-21 15:34:32.744487', 21, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'b61aef34-9f8d-4161-8b5a-d5b8488db42d', false, NULL);
INSERT INTO devices_audit VALUES ('D', '2017-12-21 15:34:54.863', 20, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '272a7292-9b75-405c-9886-e892dc8aa2f6', false, NULL);
INSERT INTO devices_audit VALUES ('D', '2017-12-21 15:34:54.883775', 21, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'b61aef34-9f8d-4161-8b5a-d5b8488db42d', false, NULL);
INSERT INTO devices_audit VALUES ('I', '2017-12-21 15:35:01.284198', 22, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c094e1ed-49b1-49b3-9b4e-3df6ce9fd3a0', false, NULL);
INSERT INTO devices_audit VALUES ('I', '2017-12-21 15:35:05.955426', 23, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '89e052b1-dd94-4f94-a702-a701837e5e46', false, NULL);
INSERT INTO devices_audit VALUES ('I', '2017-12-21 15:35:11.780088', 24, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'b04c69da-f6b9-4235-80b8-f3c6a03cc437', false, NULL);
INSERT INTO devices_audit VALUES ('D', '2017-12-21 15:35:14.809089', 23, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '89e052b1-dd94-4f94-a702-a701837e5e46', false, NULL);
INSERT INTO devices_audit VALUES ('D', '2017-12-21 15:35:14.874441', 22, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c094e1ed-49b1-49b3-9b4e-3df6ce9fd3a0', false, NULL);
INSERT INTO devices_audit VALUES ('D', '2017-12-21 15:35:20.463692', 24, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'b04c69da-f6b9-4235-80b8-f3c6a03cc437', false, NULL);
INSERT INTO devices_audit VALUES ('I', '2017-12-21 19:27:37.949617', 25, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7095ce72-e845-475d-af2b-e1a28ca3ab69', false, 'guest');
INSERT INTO devices_audit VALUES ('U', '2017-12-21 19:27:48.683748', 25, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7095ce72-e845-475d-af2b-e1a28ca3ab69', false, 'guest');
INSERT INTO devices_audit VALUES ('D', '2017-12-21 19:27:48.686312', 25, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7095ce72-e845-475d-af2b-e1a28ca3ab69', false, 'guest');
INSERT INTO devices_audit VALUES ('I', '2018-01-03 19:44:16.23966', 26, NULL, 'Nove zarizeni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '903297e8-55b3-45ba-a9a2-db685433bed1', false, 'guest');
INSERT INTO devices_audit VALUES ('U', '2018-01-28 19:00:20.964806', 2, NULL, 'Anemometr na střeše CXI 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '439988ff-c33a-44ab-aba8-39951216edbf', false, 'guest');


--
-- TOC entry 2343 (class 0 OID 16493)
-- Dependencies: 193
-- Data for Name: functions; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO functions VALUES (3, 'Ovládání portu', true, '{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL, NULL);
INSERT INTO functions VALUES (5, 'Čtení čárových kódů', false, NULL, NULL, '{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL);
INSERT INTO functions VALUES (6, 'Zobrazní textu', false, NULL, '{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL, NULL);
INSERT INTO functions VALUES (1, 'Měření', true, '{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, '{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions VALUES (4, 'Aktivace po přesažení prahu 25', false, '{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions VALUES (10, 'prvni test funkce', true, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"int":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"vstuu  p1":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, 'guest', NULL, NULL);
INSERT INTO functions VALUES (21, 'nova funkce sss', false, '{"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":"ss","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions VALUES (11, 'otevreni okna', false, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions VALUES (19, 'xxxx', false, NULL, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions VALUES (20, 'bez niceho', false, NULL, NULL, NULL, 'guest', NULL, NULL);


--
-- TOC entry 2352 (class 0 OID 16730)
-- Dependencies: 202
-- Data for Name: functions_audit; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO functions_audit VALUES ('I', '2017-12-19 18:15:17.474439', 12, 'bla bla', false, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL);
INSERT INTO functions_audit VALUES ('I', '2017-12-19 19:10:46.332834', 13, 'ahoj', false, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2017-12-19 19:16:11.230648', 9, 'aaa', false, NULL, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable1":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL, NULL);
INSERT INTO functions_audit VALUES ('D', '2017-12-19 19:16:47.267052', 8, 'prvni%^as', false, NULL, '{"bla_sds":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"xxx na #!%":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2017-12-19 19:40:23.9439', 9, 'sssss', false, NULL, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable1":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2017-12-19 19:40:57.391464', 9, 'super trooper', false, NULL, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable1":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL, NULL);
INSERT INTO functions_audit VALUES ('I', '2017-12-19 19:42:50.043549', 14, 'nova s triggerem', false, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2017-12-19 20:44:00.489509', 10, 'prvni test funkce', false, NULL, '{"int":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"vstuu  p1":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('D', '2017-12-19 20:47:22.682349', 9, 'super trooper', false, NULL, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable1":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL, NULL);
INSERT INTO functions_audit VALUES ('D', '2017-12-19 20:47:56.629957', 12, 'bla bla', false, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL);
INSERT INTO functions_audit VALUES ('D', '2017-12-19 20:48:04.970028', 14, 'nova s triggerem', false, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2017-12-19 20:48:27.341102', 10, 'prvni test funkce', true, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"int":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"vstuu  p1":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('I', '2017-12-19 20:48:41.421263', 15, 'xxxxx', false, NULL, NULL, NULL, 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2017-12-19 20:48:48.266967', 15, 'xxxxx', false, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('I', '2017-12-19 21:07:43.665396', 16, 'nova', false, NULL, NULL, NULL, 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2017-12-19 21:07:55.580099', 16, 'nova', false, NULL, NULL, NULL, 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2017-12-19 21:08:12.561792', 16, 'nova', false, NULL, NULL, NULL, 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('D', '2017-12-19 21:08:12.563669', 16, 'nova', false, NULL, NULL, NULL, 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('I', '2017-12-20 06:42:39.363491', 17, 'xxxxxxxxx', false, NULL, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable1":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL, NULL);
INSERT INTO functions_audit VALUES ('D', '2017-12-20 06:42:53.251159', 17, 'xxxxxxxxx', false, NULL, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable1":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL, NULL);
INSERT INTO functions_audit VALUES ('D', '2017-12-20 06:42:53.393236', 15, 'xxxxx', false, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('I', '2017-12-20 06:43:08.697124', 18, 'xxxxxxx', false, NULL, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable1":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2017-12-20 06:43:18.818517', 18, 'xxxxxxx', false, NULL, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable1":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL, NULL);
INSERT INTO functions_audit VALUES ('D', '2017-12-20 06:49:38.087324', 18, 'xxxxxxx', false, NULL, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable1":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, NULL, NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2017-12-31 17:29:02.796506', 11, 'otevreni okna', false, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('I', '2018-01-02 13:03:58.138095', 19, 'xxxx', false, NULL, NULL, NULL, 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2018-01-02 13:04:09.531911', 19, 'xxxx', false, NULL, '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('I', '2018-01-04 07:41:50.855057', 20, 'bez niceho', false, NULL, NULL, NULL, 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2018-01-25 12:31:01.838295', 4, 'Aktivace po přesažení prahu', false, '{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2018-01-25 12:32:28.880813', 4, 'Aktivace po přesažení prahu', false, '{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2018-01-27 20:14:25.083677', 4, 'Aktivace po přesažení prahu', false, '{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"vstupní hodnota":{"Description":"vstupní hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"výstupní hodnota":{"Description":"výstupní hodnota","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2018-01-27 20:15:15.105685', 4, 'Aktivace po přesažení prahu', false, '{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"vstupní hodnota":{"Description":"","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"výstupní hodnota":{"Description":"","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2018-01-27 22:05:59.127811', 4, 'Aktivace po přesažení prahu 25', false, '{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"vstupní hodnota":{"Description":"","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"výstupní hodnota":{"Description":"","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2018-01-27 22:06:35.843972', 4, 'Aktivace po přesažení prahu 25', false, '{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"vstupní hodnota":{"Description":"","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2018-01-27 22:07:52.730278', 4, 'Aktivace po přesažení prahu 25', false, '{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2018-01-27 22:08:28.900659', 4, 'Aktivace po přesažení prahu 25', false, '{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2018-01-28 12:18:50.308548', 1, 'Měření', true, '{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', NULL, '{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2018-01-28 12:20:52.103808', 4, 'Aktivace po přesažení prahu 25', false, '{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('U', '2018-01-28 12:54:52.763646', 4, 'Aktivace po přesažení prahu 25', false, '{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);
INSERT INTO functions_audit VALUES ('I', '2018-01-29 07:42:57.431147', 21, 'nova funkce sss', false, '{"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":"ss","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', '{"variable":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}}', 'guest', NULL, NULL);


--
-- TOC entry 2337 (class 0 OID 16452)
-- Dependencies: 187
-- Data for Name: rights; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO rights VALUES (1, 'developer', NULL);
INSERT INTO rights VALUES (2, 'administrator', NULL);


--
-- TOC entry 2354 (class 0 OID 16745)
-- Dependencies: 204
-- Data for Name: rights_audit; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO rights_audit VALUES ('I', '2018-01-28 16:45:39.349762', 1, 'developer', NULL);
INSERT INTO rights_audit VALUES ('I', '2018-01-28 16:45:51.960436', 2, 'administrator', NULL);


--
-- TOC entry 2351 (class 0 OID 16685)
-- Dependencies: 201
-- Data for Name: task_host; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO task_host VALUES (20, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host VALUES (20, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host VALUES (2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host VALUES (2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host VALUES (2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host VALUES (2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host VALUES (22, 1, 12, 1, NULL, 'guest');
INSERT INTO task_host VALUES (22, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host VALUES (24, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host VALUES (24, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host VALUES (24, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host VALUES (24, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host VALUES (23, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host VALUES (3, 1, 14, 1, NULL, 'guest');
INSERT INTO task_host VALUES (3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host VALUES (19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host VALUES (19, 2, 2, 6, NULL, 'guest');


--
-- TOC entry 2362 (class 0 OID 16781)
-- Dependencies: 212
-- Data for Name: task_host_audit; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO task_host_audit VALUES ('I', '2017-12-19 20:49:27.756667', 12, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2017-12-19 20:49:27.758829', 12, 2, 12, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2017-12-19 20:50:29.851501', 12, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2017-12-19 20:50:29.851501', 12, 2, 12, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2017-12-20 06:44:22.188647', 15, 1, 2, 1, NULL, NULL);
INSERT INTO task_host_audit VALUES ('I', '2017-12-20 06:44:22.19169', 15, 2, NULL, 4, NULL, NULL);
INSERT INTO task_host_audit VALUES ('I', '2017-12-20 06:44:22.193696', 15, 3, 12, 3, NULL, NULL);
INSERT INTO task_host_audit VALUES ('D', '2017-12-20 06:44:28.763929', 15, 1, 2, 1, NULL, NULL);
INSERT INTO task_host_audit VALUES ('D', '2017-12-20 06:44:28.763929', 15, 2, NULL, 4, NULL, NULL);
INSERT INTO task_host_audit VALUES ('D', '2017-12-20 06:44:28.763929', 15, 3, 12, 3, NULL, NULL);
INSERT INTO task_host_audit VALUES ('I', '2017-12-20 06:44:28.766007', 15, 1, 2, 1, NULL, NULL);
INSERT INTO task_host_audit VALUES ('I', '2017-12-20 06:44:28.768596', 15, 2, NULL, 4, NULL, NULL);
INSERT INTO task_host_audit VALUES ('I', '2017-12-20 06:44:28.770578', 15, 3, 12, 3, NULL, NULL);
INSERT INTO task_host_audit VALUES ('D', '2017-12-20 06:44:34.643683', 15, 1, 2, 1, NULL, NULL);
INSERT INTO task_host_audit VALUES ('D', '2017-12-20 06:44:34.643683', 15, 2, NULL, 4, NULL, NULL);
INSERT INTO task_host_audit VALUES ('D', '2017-12-20 06:44:34.643683', 15, 3, 12, 3, NULL, NULL);
INSERT INTO task_host_audit VALUES ('U', '2018-01-02 13:57:00.7754', 3, 1, 14, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-02 13:57:00.7754', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-02 14:05:56.810207', 3, 1, 14, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-02 14:05:56.810207', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-02 14:05:58.501873', 3, 1, 14, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-02 14:05:58.501873', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-02 14:06:02.271837', 3, 1, 14, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-02 14:06:05.642223', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-02 14:06:16.985609', 3, 1, 14, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-02 14:06:16.985609', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-02 14:06:16.988444', 3, 1, 14, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-02 14:06:16.988444', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-02 14:06:16.991319', 3, 1, 14, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-02 14:06:16.993517', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-04 08:00:08.309756', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-04 08:00:08.309756', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-04 08:00:08.309756', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-04 08:00:08.313028', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-04 08:00:08.313028', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-04 08:00:08.313028', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-04 08:00:08.322838', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-04 08:00:08.325898', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-04 08:00:08.328249', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-04 08:00:08.3301', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-23 11:48:40.164104', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-23 11:48:40.164104', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-23 11:48:40.164104', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-23 11:48:40.164104', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-23 11:48:40.167979', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-23 11:48:40.167979', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-23 11:48:40.167979', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-23 11:48:40.167979', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-23 11:48:40.175355', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-23 11:48:40.178473', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-23 11:48:40.180143', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-23 11:48:40.18186', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-23 15:58:53.263004', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-23 15:58:53.263004', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-23 15:58:53.263004', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-23 15:58:53.263004', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-23 15:58:53.268198', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-23 15:58:53.268198', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-23 15:58:53.268198', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-23 15:58:53.268198', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-23 15:58:53.279496', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-23 15:58:53.285256', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-23 15:58:53.287972', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-23 15:58:53.290521', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-23 17:28:12.132963', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-23 17:28:12.132963', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-23 17:28:12.132963', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-23 17:28:12.132963', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-23 17:28:12.135901', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-23 17:28:12.135901', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-23 17:28:12.135901', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-23 17:28:12.135901', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-23 17:28:12.138792', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-23 17:28:12.142054', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-23 17:28:12.144178', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-23 17:28:12.14589', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-25 12:33:24.515479', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-25 12:33:24.515479', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-25 12:33:24.515479', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-25 12:33:24.515479', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-25 12:33:24.518977', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-25 12:33:24.518977', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-25 12:33:24.518977', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-25 12:33:24.518977', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-25 12:33:24.527958', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-25 12:33:24.530925', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-25 12:33:24.534759', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-25 12:33:24.536364', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-25 16:58:43.646564', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-25 16:58:43.646564', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-25 16:58:43.646564', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-25 16:58:43.646564', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-25 16:58:43.649516', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-25 16:58:43.649516', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-25 16:58:43.649516', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-25 16:58:43.649516', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-25 16:58:43.651903', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-25 16:58:43.654086', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-25 16:58:43.655744', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-25 16:58:43.65752', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-25 16:59:26.932484', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-25 16:59:26.932484', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-25 16:59:26.932484', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-25 16:59:26.932484', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-25 16:59:26.934879', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-25 16:59:26.934879', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-25 16:59:26.934879', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-25 16:59:26.934879', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-25 16:59:26.936948', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-25 16:59:26.93918', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-25 16:59:26.94083', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-25 16:59:26.942526', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-25 16:59:26.944134', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-26 19:21:04.862459', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-26 19:21:04.866559', 19, 2, 12, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-26 20:05:15.661014', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-26 20:05:15.661014', 19, 2, 12, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-26 20:05:15.663667', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-26 20:05:15.663667', 19, 2, 12, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-26 20:05:15.666467', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-26 20:05:15.669616', 19, 2, 12, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 13:51:01.45371', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 13:51:01.45371', 19, 2, 12, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 13:51:01.457948', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 13:51:01.457948', 19, 2, 12, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 13:51:01.471064', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 13:51:01.47543', 19, 2, 12, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:03:52.430856', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:03:52.430856', 19, 2, 12, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:03:52.434637', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:03:52.434637', 19, 2, 12, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:03:52.452635', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:03:52.45709', 19, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:24:56.833175', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:24:56.833175', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:24:56.833175', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:24:56.833175', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:24:56.833175', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:24:56.837112', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:24:56.837112', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:24:56.837112', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:24:56.837112', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:24:56.837112', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:24:56.840081', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:24:56.842158', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:24:56.843741', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:24:56.84544', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:24:56.847483', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:26:09.275778', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:26:09.275778', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:26:09.275778', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:26:09.275778', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:26:09.275778', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:26:09.278504', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:26:09.278504', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:26:09.278504', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:26:09.278504', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:26:09.278504', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:26:09.285139', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:26:09.287781', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:26:09.289662', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:26:09.294993', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:26:09.297162', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:27:37.49795', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:27:37.49795', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:27:37.49795', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:27:37.49795', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:27:37.49795', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:27:37.502263', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:27:37.502263', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:27:37.502263', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:27:37.502263', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:27:37.502263', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:27:37.504588', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:27:37.506426', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:27:37.508275', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:27:37.510283', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:27:37.512062', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:27:49.1523', 3, 1, 14, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:27:49.1523', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:27:49.156609', 3, 1, 14, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:27:49.156609', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:27:49.158985', 3, 1, 14, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:27:49.162011', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:27:58.983183', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:27:58.983183', 19, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:27:58.986383', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:27:58.986383', 19, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:27:58.988978', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:27:58.991241', 19, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:28:08.092787', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:28:08.092787', 19, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:28:08.095113', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:28:08.095113', 19, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:28:08.097534', 19, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:28:08.100475', 19, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:37:30.806377', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:37:30.806377', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:37:30.806377', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:37:30.806377', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 15:37:30.806377', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:37:30.809376', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:37:30.809376', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:37:30.809376', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:37:30.809376', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 15:37:30.809376', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:37:30.812717', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:37:30.815283', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:37:30.817938', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:37:30.820269', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 15:37:30.822194', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 16:24:54.286267', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 16:24:54.286267', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 16:24:54.286267', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 16:24:54.286267', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 16:24:54.286267', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 16:24:54.289085', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 16:24:54.289085', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 16:24:54.289085', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 16:24:54.289085', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 16:24:54.289085', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 16:24:54.291293', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 16:24:54.293614', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 16:24:54.295575', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 16:24:54.297549', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 16:24:54.299803', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 16:27:55.319451', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 16:27:55.319451', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 16:27:55.319451', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 16:27:55.319451', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 16:27:55.319451', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 16:27:55.322719', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 16:27:55.322719', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 16:27:55.322719', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 16:27:55.322719', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 16:27:55.322719', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 16:27:55.325502', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 16:27:55.327928', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 16:27:55.329902', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 16:27:55.332067', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 16:27:55.334897', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 19:55:32.62006', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 19:55:32.62006', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 19:55:32.62006', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 19:55:32.62006', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 19:55:32.62006', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 19:55:32.62366', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 19:55:32.62366', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 19:55:32.62366', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 19:55:32.62366', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 19:55:32.62366', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 19:55:32.626272', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 19:55:32.629003', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 19:55:32.630715', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 19:55:32.632545', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 19:55:32.634173', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 19:55:56.693268', 3, 1, 14, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 19:55:56.693268', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 19:55:56.695858', 3, 1, 14, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 19:55:56.695858', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 19:55:56.698088', 3, 1, 14, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 19:55:56.700837', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 20:49:28.170853', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 20:49:28.170853', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 20:49:28.170853', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 20:49:28.170853', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 20:49:28.170853', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 20:49:28.174469', 2, 1, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 20:49:28.174469', 2, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 20:49:28.174469', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 20:49:28.174469', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 20:49:28.174469', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 20:49:28.177339', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 20:49:28.18004', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 20:49:28.182705', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 20:49:28.18653', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:07:15.774593', 20, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:07:15.777267', 20, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 21:07:23.77845', 20, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 21:07:23.77845', 20, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 21:07:23.780822', 20, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 21:07:23.780822', 20, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:07:23.783076', 20, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:07:23.78563', 20, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 21:07:30.026849', 20, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 21:07:30.026849', 20, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 21:07:30.029258', 20, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 21:07:30.029258', 20, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:07:30.03128', 20, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:07:30.033564', 20, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:08:58.323063', 21, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:08:58.325956', 21, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 21:09:07.275092', 21, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 21:09:07.275092', 21, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 21:09:07.277815', 21, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 21:09:07.277815', 21, 2, 2, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:10:03.476634', 22, 1, 12, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:10:03.479158', 22, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:10:03.481463', 22, 3, 12, 10, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 21:10:22.442021', 22, 1, 12, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 21:10:22.442021', 22, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 21:10:22.442021', 22, 3, 12, 10, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 21:10:22.444774', 22, 1, 12, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 21:10:22.444774', 22, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 21:10:22.444774', 22, 3, 12, 10, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:10:22.447407', 22, 1, 12, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:10:22.449782', 22, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 21:10:27.973984', 22, 1, 12, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 21:10:27.973984', 22, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 21:10:27.976272', 22, 1, 12, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 21:10:27.976272', 22, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:10:27.978355', 22, 1, 12, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:10:27.980321', 22, 2, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 21:20:29.62297', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 21:20:29.62297', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 21:20:29.62297', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 21:20:29.62297', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 21:20:29.626424', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 21:20:29.626424', 2, 4, NULL, 11, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 21:20:29.626424', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 21:20:29.626424', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:20:29.633607', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:20:29.636379', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:20:29.638623', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 21:20:29.640536', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 22:12:53.621719', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 22:12:53.621719', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 22:12:53.621719', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-27 22:12:53.621719', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 22:12:53.624706', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 22:12:53.624706', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 22:12:53.624706', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-27 22:12:53.624706', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 22:12:53.632503', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 22:12:53.635299', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 22:12:53.63723', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-27 22:12:53.639118', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:18:41.104504', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:18:41.104504', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:18:41.104504', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:18:41.104504', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:18:41.10897', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:18:41.10897', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:18:41.10897', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:18:41.10897', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:18:41.11695', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:18:41.120055', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:18:41.121725', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:18:41.123557', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:19:08.06297', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:19:08.06297', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:19:08.06297', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:19:08.06297', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:19:08.06638', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:19:08.06638', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:19:08.06638', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:19:08.06638', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:19:08.069245', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:19:08.071734', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:19:08.07372', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:19:08.075986', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:21:58.487822', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:21:58.487822', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:21:58.487822', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:21:58.487822', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:21:58.490893', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:21:58.490893', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:21:58.490893', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:21:58.490893', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:21:58.493726', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:21:58.495969', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:21:58.497989', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:21:58.499784', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:22:24.627448', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:22:24.627448', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:22:24.627448', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:22:24.627448', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:22:24.630654', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:22:24.630654', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:22:24.630654', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:22:24.630654', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:22:24.633554', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:22:24.635756', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:22:24.637904', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:22:24.639855', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:23:10.569294', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:23:10.569294', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:23:10.569294', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:23:10.569294', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:23:10.572783', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:23:10.572783', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:23:10.572783', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:23:10.572783', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:23:10.57605', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:23:10.578593', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:23:10.580626', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:23:10.582578', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:53:49.846694', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:53:49.846694', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:53:49.846694', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:53:49.846694', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:53:49.849986', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:53:49.849986', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:53:49.849986', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:53:49.849986', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:53:49.857043', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:53:49.859668', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:53:49.861855', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:53:49.863904', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:55:55.697455', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:55:55.697455', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:55:55.697455', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-28 12:55:55.697455', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:55:55.699775', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:55:55.699775', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:55:55.699775', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-28 12:55:55.699775', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:55:55.701848', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:55:55.703873', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:55:55.705555', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-28 12:55:55.70696', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:34:07.498411', 23, 1, 15, 5, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:43:31.809363', 3, 1, 14, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:43:31.809363', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:43:31.812303', 3, 1, 14, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:43:31.812303', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:43:31.815011', 3, 1, 14, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:43:31.817434', 3, 2, 15, 6, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:45:47.149667', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:45:47.149667', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:45:47.149667', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:45:47.149667', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:45:47.153263', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:45:47.153263', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:45:47.153263', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:45:47.153263', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:45:47.155628', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:45:47.157841', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:45:47.159755', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:45:47.161722', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:46:11.905012', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:46:11.905012', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:46:11.905012', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:46:11.905012', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:46:11.907354', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:46:11.907354', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:46:11.907354', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:46:11.907354', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:46:11.909457', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:46:11.911365', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:46:11.91308', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:46:11.915009', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:47:42.456092', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:47:42.456092', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:47:42.456092', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:47:42.456092', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:47:42.459661', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:47:42.459661', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:47:42.459661', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:47:42.459661', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:47:42.462555', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:47:42.464964', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:47:42.466696', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:47:42.468395', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:48:06.854996', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:48:06.854996', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:48:06.854996', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 07:48:06.854996', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:48:06.857655', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:48:06.857655', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:48:06.857655', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 07:48:06.857655', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:48:06.860131', 2, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:48:06.862325', 2, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:48:06.872976', 2, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 07:48:06.87492', 2, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 08:07:13.458079', 24, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 08:07:13.460687', 24, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 08:07:13.462597', 24, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 08:07:13.464217', 24, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 08:07:18.46883', 24, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 08:07:18.46883', 24, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 08:07:18.46883', 24, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('U', '2018-01-29 08:07:18.46883', 24, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 08:07:18.471234', 24, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 08:07:18.471234', 24, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 08:07:18.471234', 24, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('D', '2018-01-29 08:07:18.471234', 24, 6, 2, 1, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 08:07:18.474429', 24, 3, 12, 3, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 08:07:18.47652', 24, 4, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 08:07:18.47847', 24, 5, NULL, 4, NULL, 'guest');
INSERT INTO task_host_audit VALUES ('I', '2018-01-29 08:07:18.480312', 24, 6, 2, 1, NULL, 'guest');


--
-- TOC entry 2345 (class 0 OID 16499)
-- Dependencies: 195
-- Data for Name: tasks; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO tasks VALUES (1, 'xxxxx', true, true, NULL, 0, false, NULL, NULL);
INSERT INTO tasks VALUES (4, 'kopie Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, true, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":"20","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, NULL, NULL);
INSERT INTO tasks VALUES (9, 'kopie kopie kopie kopie Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, true, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":"20","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, NULL, NULL);
INSERT INTO tasks VALUES (5, 'kopie kopie Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, true, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":"20","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, NULL, NULL);
INSERT INTO tasks VALUES (6, 'kopie kopie Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, true, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":"20","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, NULL, NULL);
INSERT INTO tasks VALUES (7, 'kopie kopie Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, true, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":"20","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, NULL, NULL);
INSERT INTO tasks VALUES (8, 'kopie kopie kopie Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, true, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":"20","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, NULL, NULL);
INSERT INTO tasks VALUES (10, 'kopie Zobraz tag na lince, pokud je načten', false, true, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, NULL, NULL);
INSERT INTO tasks VALUES (11, 'kopie Zobraz tag na lince, pokud je načten', false, true, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, NULL, NULL);
INSERT INTO tasks VALUES (21, 'aaaaaaa', false, true, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', 'kopie kopie aaaaaaa');
INSERT INTO tasks VALUES (22, NULL, true, false, '{"1":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 2, false, 'guest', 'nova');
INSERT INTO tasks VALUES (2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 33, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks VALUES (20, 'aaaaaaa', true, false, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 2, false, 'guest', 'kopie aaaaaaa');
INSERT INTO tasks VALUES (12, 'privni s logovanim', false, true, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', NULL);
INSERT INTO tasks VALUES (3, 'Zobraz tag na lince, pokud je načten 555', true, false, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"24","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 17, false, 'guest', 'Zobraz tag na lince, pokud je načten 555');
INSERT INTO tasks VALUES (24, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', false, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 1, false, 'guest', 'kopie Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks VALUES (13, 'xxxxx', true, true, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', NULL);
INSERT INTO tasks VALUES (14, 'kopie xxxxx', false, true, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', NULL);
INSERT INTO tasks VALUES (15, 'sadasdasd', false, true, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 1, false, NULL, NULL);
INSERT INTO tasks VALUES (16, NULL, false, true, NULL, 0, false, NULL, NULL);
INSERT INTO tasks VALUES (17, 'xxxx', false, true, NULL, 0, false, 'guest', NULL);
INSERT INTO tasks VALUES (18, 'xxxxxxxxx', true, true, NULL, 0, false, 'guest', NULL);
INSERT INTO tasks VALUES (19, 'aaaaaaa', true, false, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 5, false, 'guest', 'aaaaaaa');
INSERT INTO tasks VALUES (23, NULL, false, false, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 1, false, 'guest', 'delsi nova');


--
-- TOC entry 2357 (class 0 OID 16760)
-- Dependencies: 207
-- Data for Name: tasks_audit; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO tasks_audit VALUES ('U', '2017-12-19 20:40:08.398525', 3, 'Zobraz tag na lince, pokud je načten', true, false, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 5, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('I', '2017-12-19 20:49:27.751921', 12, 'privni s logovanim', false, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2017-12-19 20:50:29.842028', 12, 'privni s logovanim', false, true, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('I', '2017-12-19 21:09:12.966906', 13, 'xxxxx', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('I', '2017-12-19 21:09:29.453444', 14, 'kopie xxxxx', false, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2017-12-19 21:09:44.477058', 13, 'xxxxx', true, true, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2017-12-19 21:10:08.687188', 14, 'kopie xxxxx', false, true, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('I', '2017-12-20 06:44:22.17628', 15, NULL, false, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, NULL, NULL);
INSERT INTO tasks_audit VALUES ('U', '2017-12-20 06:44:28.75982', 15, 'sadasdasd', false, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 1, false, NULL, NULL);
INSERT INTO tasks_audit VALUES ('U', '2017-12-20 06:44:34.639234', 15, 'sadasdasd', false, true, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 1, false, NULL, NULL);
INSERT INTO tasks_audit VALUES ('I', '2017-12-21 15:35:27.237606', 16, NULL, false, false, NULL, 0, false, NULL, NULL);
INSERT INTO tasks_audit VALUES ('U', '2017-12-21 15:35:37.353554', 16, NULL, false, true, NULL, 0, false, NULL, NULL);
INSERT INTO tasks_audit VALUES ('I', '2017-12-21 15:54:16.641986', 17, 'xxxx', false, false, NULL, 0, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2017-12-21 15:54:20.997122', 17, 'xxxx', false, true, NULL, 0, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2017-12-21 20:08:36.18497', 3, 'Zobraz tag na lince, pokud je načten 555', true, false, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 6, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2017-12-31 17:28:30.118895', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":"20","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 4, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2017-12-31 17:29:13.187297', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":"20","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 5, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-02 13:04:28.629192', 3, 'Zobraz tag na lince, pokud je načten 555', true, false, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 7, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-02 13:05:22.760648', 3, 'Zobraz tag na lince, pokud je načten 555', true, false, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 8, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-02 13:05:57.254376', 3, 'Zobraz tag na lince, pokud je načten 555', true, false, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 9, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-02 13:06:08.338622', 3, 'Zobraz tag na lince, pokud je načten 555', true, false, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 10, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-02 13:06:32.724967', 3, 'Zobraz tag na lince, pokud je načten 555', true, false, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 11, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-02 13:07:21.113064', 3, 'Zobraz tag na lince, pokud je načten 555', true, false, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 12, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-02 14:05:55.416753', 3, 'Zobraz tag na lince, pokud je načten 555', true, false, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 13, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-02 14:06:16.98108', 3, 'Zobraz tag na lince, pokud je načten 555', true, false, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 14, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-04 08:00:08.303043', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":"20","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 6, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-23 11:48:40.152207', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":"20","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 7, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-23 15:58:53.251363', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":"20","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 8, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-23 17:28:12.126806', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":"20","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 9, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-25 12:33:24.507397', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 10, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-25 16:58:43.64063', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 11, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-25 16:59:26.928177', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 12, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('I', '2018-01-26 19:20:33.199813', 18, 'xxxxxxxxx', true, false, NULL, 0, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('I', '2018-01-26 19:21:04.850418', 19, 'aaaaaaa', false, false, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-26 20:05:15.656036', 19, 'aaaaaaa', true, false, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 1, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-26 20:05:26.641373', 18, 'xxxxxxxxx', true, true, NULL, 0, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 13:51:01.440234', 19, 'aaaaaaa', true, false, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 2, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 15:03:52.4211', 19, 'aaaaaaa', true, false, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 3, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 15:24:56.827125', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 13, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 15:26:09.266844', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 14, false, 'guest', NULL);
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 15:27:34.484292', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 15, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 15:27:49.147498', 3, 'Zobraz tag na lince, pokud je načten 555', true, false, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 15, false, 'guest', 'Zobraz tag na lince, pokud je načten 555');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 15:27:58.97878', 19, 'aaaaaaa', true, false, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 4, false, 'guest', 'aaaaaaa');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 15:28:00.732981', 19, 'aaaaaaa', false, false, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 4, false, 'guest', 'aaaaaaa');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 15:28:08.088633', 19, 'aaaaaaa', true, false, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 5, false, 'guest', 'aaaaaaa');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 15:37:30.801689', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 16, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 16:24:54.280714', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":"7","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 17, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 16:27:55.314272', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":2,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 18, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 19:55:32.573843', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"1":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"variable"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 19, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 19:55:56.688816', 3, 'Zobraz tag na lince, pokud je načten 555', true, false, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"22","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 16, false, 'guest', 'Zobraz tag na lince, pokud je načten 555');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 20:49:28.163058', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"variable"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":11,"Description":"otevreni okna","ImagePath":null,"Local":false,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"variable7":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"variable":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 20, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('I', '2018-01-27 21:07:15.735034', 20, 'aaaaaaa', false, false, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', 'kopie aaaaaaa');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 21:07:23.774047', 20, 'aaaaaaa', false, false, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 1, false, 'guest', 'kopie aaaaaaa');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 21:07:30.021832', 20, 'aaaaaaa', true, false, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 2, false, 'guest', 'kopie aaaaaaa');
INSERT INTO tasks_audit VALUES ('I', '2018-01-27 21:08:58.314812', 21, 'aaaaaaa', false, false, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', 'kopie kopie aaaaaaa');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 21:09:07.270854', 21, 'aaaaaaa', false, true, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"ćárový kód"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', 'kopie kopie aaaaaaa');
INSERT INTO tasks_audit VALUES ('I', '2018-01-27 21:10:03.469427', 22, NULL, false, false, '{"1":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":10,"Description":"prvni test funkce","ImagePath":null,"Local":true,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"int":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"vstuu  p1":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', 'nova');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 21:10:06.798507', 22, NULL, true, false, '{"1":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":10,"Description":"prvni test funkce","ImagePath":null,"Local":true,"Parameters":{"variable":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"int":{"Description":null,"Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"vstuu  p1":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 0, false, 'guest', 'nova');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 21:10:22.437173', 22, NULL, true, false, '{"1":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 1, false, 'guest', 'nova');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 21:10:27.970075', 22, NULL, true, false, '{"1":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":1,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 2, false, 'guest', 'nova');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 21:20:29.615325', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 21, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-27 22:12:53.617004', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"144","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 22, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-28 12:18:41.048608', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 23, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-28 12:19:08.053088', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 24, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-28 12:21:58.481362', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 25, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-28 12:22:24.620423', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 26, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-28 12:23:10.564359', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 27, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-28 12:53:49.837913', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 28, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-28 12:55:55.693176', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 29, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-28 15:21:40.361511', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', false, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 29, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-28 15:21:41.488769', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 29, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-28 15:21:42.47115', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', false, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 29, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-28 15:21:43.114995', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 29, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('I', '2018-01-29 07:33:37.536953', 23, NULL, false, false, NULL, 0, false, 'guest', 'delsi nova');
INSERT INTO tasks_audit VALUES ('U', '2018-01-29 07:34:07.485517', 23, NULL, false, false, '{"1":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":5,"Description":"Čtení čárových kódů","ImagePath":null,"Local":false,"Parameters":null,"Inputs":null,"Outputs":{"ćárový kód":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 1, false, 'guest', 'delsi nova');
INSERT INTO tasks_audit VALUES ('U', '2018-01-29 07:43:31.804763', 3, 'Zobraz tag na lince, pokud je načten 555', true, false, '{"1":{"Device":{"Id":14,"GroupName":null,"GroupId":null,"Description":"Čtečka čárových kódů","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"24","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"2":{"Device":{"Id":15,"GroupName":"tul","GroupId":null,"Description":"Monitor na lince","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":6,"Description":"Zobrazní textu","ImagePath":null,"Local":false,"Parameters":null,"Inputs":{"text":{"Description":null,"Type":3,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}}}', 17, false, 'guest', 'Zobraz tag na lince, pokud je načten 555');
INSERT INTO tasks_audit VALUES ('U', '2018-01-29 07:45:47.145632', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 30, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-29 07:46:11.901168', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 31, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-29 07:47:42.451768', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 32, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-29 07:48:06.851081', 2, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', true, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 33, false, 'guest', 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('I', '2018-01-29 08:07:13.451218', 24, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', false, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 0, false, 'guest', 'kopie Vytáhni rolety, pokud je vítr vyšší než 20 m/s');
INSERT INTO tasks_audit VALUES ('U', '2018-01-29 08:07:18.464021', 24, 'Vytáhni rolety, pokud je vítr vyšší než 20 m/s a jeste neco navic', false, false, '{"3":{"Device":{"Id":12,"GroupName":null,"GroupId":null,"Description":"Ovladač rolet","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":3,"Description":"Ovládání portu","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"Číslo ovládaného portu","Type":1,"Value":"1","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"aktivace portu":{"Description":"aktivace portu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":5,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":null,"Versions":null,"Stored":false}},"4":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":6,"VariableKey":"Měřená hodnota"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"5":{"Device":null,"Function":{"Id":4,"Description":"Aktivace po přesažení prahu 25","ImagePath":null,"Local":false,"Parameters":{"práh":{"Description":"Práh nad kterým je aktivován výstup 25","Type":2,"Value":"233","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"invertovat":{"Description":"invertovat vystup","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":{"vstupní hodnota":{"Description":"vstupní hodnota vice popisu","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"kopie vstupu"},"Stored":false},"blokovaci vstup":{"Description":null,"Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":{"HostKey":4,"VariableKey":"výstupní hodnota"},"Stored":false}},"Outputs":{"výstupní hodnota":{"Description":"výstupní hodnota vice popisu","Type":0,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false},"kopie vstupu":{"Description":null,"Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}},"6":{"Device":{"Id":2,"GroupName":null,"GroupId":null,"Description":"Anemometr na střeše CXI","Online":null,"Updates":false,"Version":null,"LastOnline":null,"Ip":null,"FirmwareVersion":null,"Uuid":null,"Stored":false},"Function":{"Id":1,"Description":"Měření","ImagePath":null,"Local":true,"Parameters":{"port":{"Description":"port na kterém se měří hodnota","Type":1,"Value":"8","DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Inputs":null,"Outputs":{"Měřená hodnota":{"Description":"Měřená hodnota vcera a dnes","Type":2,"Value":null,"DefaultValue":null,"EnumValues":null,"DataSource":null,"Stored":false}},"Versions":null,"Stored":false}}}', 1, false, 'guest', 'kopie Vytáhni rolety, pokud je vítr vyšší než 20 m/s');


--
-- TOC entry 2347 (class 0 OID 16506)
-- Dependencies: 197
-- Data for Name: user_group; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO user_group VALUES (1, 1, NULL);


--
-- TOC entry 2359 (class 0 OID 16769)
-- Dependencies: 209
-- Data for Name: user_group_audit; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2346 (class 0 OID 16503)
-- Dependencies: 196
-- Data for Name: user_right; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO user_right VALUES (0, 1, NULL);
INSERT INTO user_right VALUES (0, 2, NULL);
INSERT INTO user_right VALUES (1, 1, NULL);
INSERT INTO user_right VALUES (1, 2, NULL);
INSERT INTO user_right VALUES (2, 1, NULL);
INSERT INTO user_right VALUES (3, 1, NULL);


--
-- TOC entry 2358 (class 0 OID 16766)
-- Dependencies: 208
-- Data for Name: user_right_audit; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO user_right_audit VALUES ('I', '2018-01-28 16:53:38.831936', 1, 2, NULL);
INSERT INTO user_right_audit VALUES ('I', '2018-01-28 16:53:46.38931', 1, 1, NULL);
INSERT INTO user_right_audit VALUES ('D', '2018-01-28 17:37:31.488585', 1, 1, NULL);
INSERT INTO user_right_audit VALUES ('D', '2018-01-28 17:37:31.488585', 1, 2, NULL);
INSERT INTO user_right_audit VALUES ('I', '2018-01-28 17:38:22.06561', 1, 1, NULL);
INSERT INTO user_right_audit VALUES ('I', '2018-01-28 17:38:25.978443', 1, 2, NULL);
INSERT INTO user_right_audit VALUES ('D', '2018-01-28 17:44:48.194941', 1, 1, NULL);
INSERT INTO user_right_audit VALUES ('D', '2018-01-28 17:44:48.194941', 1, 2, NULL);
INSERT INTO user_right_audit VALUES ('I', '2018-01-28 18:16:01.608091', 1, 2, NULL);
INSERT INTO user_right_audit VALUES ('I', '2018-01-28 18:16:05.808743', 1, 1, NULL);


--
-- TOC entry 2336 (class 0 OID 16443)
-- Dependencies: 186
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO users VALUES (0, 'guest', NULL, NULL, 'guest', '6acd7a3c-4330-4f73-a379-82138bd000c0', NULL);
INSERT INTO users VALUES (1, 'guest1', NULL, NULL, 'guest1', '6acd7a3c-4330-4f73-a379-82138bd000c1', NULL);
INSERT INTO users VALUES (2, 'guest2', NULL, NULL, 'guest2', '6acd7a3c-4330-4f73-a379-82138bd000c2', NULL);
INSERT INTO users VALUES (3, 'guest3', NULL, NULL, 'guest3', '6acd7a3c-4330-4f73-a379-82138bd000c3', NULL);


--
-- TOC entry 2353 (class 0 OID 16739)
-- Dependencies: 203
-- Data for Name: users_audit; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2350 (class 0 OID 16550)
-- Dependencies: 200
-- Data for Name: version_state; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO version_state VALUES (1, 'Preliminary', NULL);
INSERT INTO version_state VALUES (2, 'Active', NULL);
INSERT INTO version_state VALUES (3, 'Deprecated', NULL);
INSERT INTO version_state VALUES (4, 'Removed', NULL);


--
-- TOC entry 2361 (class 0 OID 16778)
-- Dependencies: 211
-- Data for Name: version_state_audit; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2349 (class 0 OID 16539)
-- Dependencies: 199
-- Data for Name: versions; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO versions VALUES (4, 1, '777', NULL, 2, '\xefbbbf0d0a4d6963726f736f66742056697375616c2053747564696f20536f6c7574696f6e2046696c652c20466f726d61742056657273696f6e2031322e30300d0a232056697375616c2053747564696f2031350d0a56697375616c53747564696f56657273696f6e203d2031352e302e32363430332e370d0a4d696e696d756d56697375616c53747564696f56657273696f6e203d2031302e302e34303231392e310d0a50726f6a65637428227b46414530344543302d333031462d313144332d424634422d3030433034463739454642437d2229203d2022536572766963654d6f64654f7470222c2022536572766963654d6f64654f74705c536572766963654d6f64654f74702e637370726f6a222c20227b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d220d0a456e6450726f6a6563740d0a476c6f62616c0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e436f6e66696775726174696f6e506c6174666f726d7329203d20707265536f6c7574696f6e0d0a090944656275677c416e7920435055203d2044656275677c416e79204350550d0a090952656c656173657c416e7920435055203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e2850726f6a656374436f6e66696775726174696f6e506c6174666f726d7329203d20706f7374536f6c7574696f6e0d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e416374697665436667203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e4275696c642e30203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e416374697665436667203d2052656c656173657c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e4275696c642e30203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e50726f7065727469657329203d20707265536f6c7574696f6e0d0a090948696465536f6c7574696f6e4e6f6465203d2046414c53450d0a09456e64476c6f62616c53656374696f6e0d0a456e64476c6f62616c0d0a', 'guest');
INSERT INTO versions VALUES (5, 1, '778', NULL, 1, '\xefbbbf0d0a4d6963726f736f66742056697375616c2053747564696f20536f6c7574696f6e2046696c652c20466f726d61742056657273696f6e2031322e30300d0a232056697375616c2053747564696f2031350d0a56697375616c53747564696f56657273696f6e203d2031352e302e32363430332e370d0a4d696e696d756d56697375616c53747564696f56657273696f6e203d2031302e302e34303231392e310d0a50726f6a65637428227b46414530344543302d333031462d313144332d424634422d3030433034463739454642437d2229203d2022536572766963654d6f64654f7470222c2022536572766963654d6f64654f74705c536572766963654d6f64654f74702e637370726f6a222c20227b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d220d0a456e6450726f6a6563740d0a476c6f62616c0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e436f6e66696775726174696f6e506c6174666f726d7329203d20707265536f6c7574696f6e0d0a090944656275677c416e7920435055203d2044656275677c416e79204350550d0a090952656c656173657c416e7920435055203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e2850726f6a656374436f6e66696775726174696f6e506c6174666f726d7329203d20706f7374536f6c7574696f6e0d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e416374697665436667203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e4275696c642e30203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e416374697665436667203d2052656c656173657c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e4275696c642e30203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e50726f7065727469657329203d20707265536f6c7574696f6e0d0a090948696465536f6c7574696f6e4e6f6465203d2046414c53450d0a09456e64476c6f62616c53656374696f6e0d0a456e64476c6f62616c0d0a', 'guest');
INSERT INTO versions VALUES (6, 1, '779', NULL, 3, '\xefbbbf0d0a4d6963726f736f66742056697375616c2053747564696f20536f6c7574696f6e2046696c652c20466f726d61742056657273696f6e2031322e30300d0a232056697375616c2053747564696f2031350d0a56697375616c53747564696f56657273696f6e203d2031352e302e32363430332e370d0a4d696e696d756d56697375616c53747564696f56657273696f6e203d2031302e302e34303231392e310d0a50726f6a65637428227b46414530344543302d333031462d313144332d424634422d3030433034463739454642437d2229203d2022536572766963654d6f64654f7470222c2022536572766963654d6f64654f74705c536572766963654d6f64654f74702e637370726f6a222c20227b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d220d0a456e6450726f6a6563740d0a476c6f62616c0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e436f6e66696775726174696f6e506c6174666f726d7329203d20707265536f6c7574696f6e0d0a090944656275677c416e7920435055203d2044656275677c416e79204350550d0a090952656c656173657c416e7920435055203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e2850726f6a656374436f6e66696775726174696f6e506c6174666f726d7329203d20706f7374536f6c7574696f6e0d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e416374697665436667203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e4275696c642e30203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e416374697665436667203d2052656c656173657c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e4275696c642e30203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e50726f7065727469657329203d20707265536f6c7574696f6e0d0a090948696465536f6c7574696f6e4e6f6465203d2046414c53450d0a09456e64476c6f62616c53656374696f6e0d0a456e64476c6f62616c0d0a', 'guest');


--
-- TOC entry 2360 (class 0 OID 16772)
-- Dependencies: 210
-- Data for Name: versions_audit; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO versions_audit VALUES ('D', '2017-12-19 20:47:19.468282', 7, 9, '1', NULL, 1, '\xefbbbf0d0a4d6963726f736f66742056697375616c2053747564696f20536f6c7574696f6e2046696c652c20466f726d61742056657273696f6e2031322e30300d0a232056697375616c2053747564696f2031350d0a56697375616c53747564696f56657273696f6e203d2031352e302e32363430332e370d0a4d696e696d756d56697375616c53747564696f56657273696f6e203d2031302e302e34303231392e310d0a50726f6a65637428227b46414530344543302d333031462d313144332d424634422d3030433034463739454642437d2229203d2022536572766963654d6f64654f7470222c2022536572766963654d6f64654f74705c536572766963654d6f64654f74702e637370726f6a222c20227b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d220d0a456e6450726f6a6563740d0a476c6f62616c0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e436f6e66696775726174696f6e506c6174666f726d7329203d20707265536f6c7574696f6e0d0a090944656275677c416e7920435055203d2044656275677c416e79204350550d0a090952656c656173657c416e7920435055203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e2850726f6a656374436f6e66696775726174696f6e506c6174666f726d7329203d20706f7374536f6c7574696f6e0d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e416374697665436667203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e4275696c642e30203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e416374697665436667203d2052656c656173657c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e4275696c642e30203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e50726f7065727469657329203d20707265536f6c7574696f6e0d0a090948696465536f6c7574696f6e4e6f6465203d2046414c53450d0a09456e64476c6f62616c53656374696f6e0d0a456e64476c6f62616c0d0a', NULL);
INSERT INTO versions_audit VALUES ('I', '2017-12-19 21:07:55.592799', 8, 16, '1', NULL, 1, '\xefbbbf0d0a4d6963726f736f66742056697375616c2053747564696f20536f6c7574696f6e2046696c652c20466f726d61742056657273696f6e2031322e30300d0a232056697375616c2053747564696f2031350d0a56697375616c53747564696f56657273696f6e203d2031352e302e32363430332e370d0a4d696e696d756d56697375616c53747564696f56657273696f6e203d2031302e302e34303231392e310d0a50726f6a65637428227b46414530344543302d333031462d313144332d424634422d3030433034463739454642437d2229203d2022536572766963654d6f64654f7470222c2022536572766963654d6f64654f74705c536572766963654d6f64654f74702e637370726f6a222c20227b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d220d0a456e6450726f6a6563740d0a476c6f62616c0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e436f6e66696775726174696f6e506c6174666f726d7329203d20707265536f6c7574696f6e0d0a090944656275677c416e7920435055203d2044656275677c416e79204350550d0a090952656c656173657c416e7920435055203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e2850726f6a656374436f6e66696775726174696f6e506c6174666f726d7329203d20706f7374536f6c7574696f6e0d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e416374697665436667203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e4275696c642e30203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e416374697665436667203d2052656c656173657c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e4275696c642e30203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e50726f7065727469657329203d20707265536f6c7574696f6e0d0a090948696465536f6c7574696f6e4e6f6465203d2046414c53450d0a09456e64476c6f62616c53656374696f6e0d0a456e64476c6f62616c0d0a', 'guest');
INSERT INTO versions_audit VALUES ('U', '2017-12-19 21:08:12.556649', 8, 16, '1', NULL, 1, '\xefbbbf0d0a4d6963726f736f66742056697375616c2053747564696f20536f6c7574696f6e2046696c652c20466f726d61742056657273696f6e2031322e30300d0a232056697375616c2053747564696f2031350d0a56697375616c53747564696f56657273696f6e203d2031352e302e32363430332e370d0a4d696e696d756d56697375616c53747564696f56657273696f6e203d2031302e302e34303231392e310d0a50726f6a65637428227b46414530344543302d333031462d313144332d424634422d3030433034463739454642437d2229203d2022536572766963654d6f64654f7470222c2022536572766963654d6f64654f74705c536572766963654d6f64654f74702e637370726f6a222c20227b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d220d0a456e6450726f6a6563740d0a476c6f62616c0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e436f6e66696775726174696f6e506c6174666f726d7329203d20707265536f6c7574696f6e0d0a090944656275677c416e7920435055203d2044656275677c416e79204350550d0a090952656c656173657c416e7920435055203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e2850726f6a656374436f6e66696775726174696f6e506c6174666f726d7329203d20706f7374536f6c7574696f6e0d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e416374697665436667203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e4275696c642e30203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e416374697665436667203d2052656c656173657c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e4275696c642e30203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e50726f7065727469657329203d20707265536f6c7574696f6e0d0a090948696465536f6c7574696f6e4e6f6465203d2046414c53450d0a09456e64476c6f62616c53656374696f6e0d0a456e64476c6f62616c0d0a', 'guest');
INSERT INTO versions_audit VALUES ('D', '2017-12-19 21:08:12.559924', 8, 16, '1', NULL, 1, '\xefbbbf0d0a4d6963726f736f66742056697375616c2053747564696f20536f6c7574696f6e2046696c652c20466f726d61742056657273696f6e2031322e30300d0a232056697375616c2053747564696f2031350d0a56697375616c53747564696f56657273696f6e203d2031352e302e32363430332e370d0a4d696e696d756d56697375616c53747564696f56657273696f6e203d2031302e302e34303231392e310d0a50726f6a65637428227b46414530344543302d333031462d313144332d424634422d3030433034463739454642437d2229203d2022536572766963654d6f64654f7470222c2022536572766963654d6f64654f74705c536572766963654d6f64654f74702e637370726f6a222c20227b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d220d0a456e6450726f6a6563740d0a476c6f62616c0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e436f6e66696775726174696f6e506c6174666f726d7329203d20707265536f6c7574696f6e0d0a090944656275677c416e7920435055203d2044656275677c416e79204350550d0a090952656c656173657c416e7920435055203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e2850726f6a656374436f6e66696775726174696f6e506c6174666f726d7329203d20706f7374536f6c7574696f6e0d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e416374697665436667203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e4275696c642e30203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e416374697665436667203d2052656c656173657c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e4275696c642e30203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e50726f7065727469657329203d20707265536f6c7574696f6e0d0a090948696465536f6c7574696f6e4e6f6465203d2046414c53450d0a09456e64476c6f62616c53656374696f6e0d0a456e64476c6f62616c0d0a', 'guest');
INSERT INTO versions_audit VALUES ('I', '2017-12-20 06:43:18.827837', 9, 18, '1', NULL, 1, '\xefbbbf0d0a4d6963726f736f66742056697375616c2053747564696f20536f6c7574696f6e2046696c652c20466f726d61742056657273696f6e2031322e30300d0a232056697375616c2053747564696f2031350d0a56697375616c53747564696f56657273696f6e203d2031352e302e32363430332e370d0a4d696e696d756d56697375616c53747564696f56657273696f6e203d2031302e302e34303231392e310d0a50726f6a65637428227b46414530344543302d333031462d313144332d424634422d3030433034463739454642437d2229203d2022536572766963654d6f64654f7470222c2022536572766963654d6f64654f74705c536572766963654d6f64654f74702e637370726f6a222c20227b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d220d0a456e6450726f6a6563740d0a476c6f62616c0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e436f6e66696775726174696f6e506c6174666f726d7329203d20707265536f6c7574696f6e0d0a090944656275677c416e7920435055203d2044656275677c416e79204350550d0a090952656c656173657c416e7920435055203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e2850726f6a656374436f6e66696775726174696f6e506c6174666f726d7329203d20706f7374536f6c7574696f6e0d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e416374697665436667203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e4275696c642e30203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e416374697665436667203d2052656c656173657c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e4275696c642e30203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e50726f7065727469657329203d20707265536f6c7574696f6e0d0a090948696465536f6c7574696f6e4e6f6465203d2046414c53450d0a09456e64476c6f62616c53656374696f6e0d0a456e64476c6f62616c0d0a', NULL);
INSERT INTO versions_audit VALUES ('D', '2017-12-20 06:49:38.081363', 9, 18, '1', NULL, 1, '\xefbbbf0d0a4d6963726f736f66742056697375616c2053747564696f20536f6c7574696f6e2046696c652c20466f726d61742056657273696f6e2031322e30300d0a232056697375616c2053747564696f2031350d0a56697375616c53747564696f56657273696f6e203d2031352e302e32363430332e370d0a4d696e696d756d56697375616c53747564696f56657273696f6e203d2031302e302e34303231392e310d0a50726f6a65637428227b46414530344543302d333031462d313144332d424634422d3030433034463739454642437d2229203d2022536572766963654d6f64654f7470222c2022536572766963654d6f64654f74705c536572766963654d6f64654f74702e637370726f6a222c20227b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d220d0a456e6450726f6a6563740d0a476c6f62616c0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e436f6e66696775726174696f6e506c6174666f726d7329203d20707265536f6c7574696f6e0d0a090944656275677c416e7920435055203d2044656275677c416e79204350550d0a090952656c656173657c416e7920435055203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e2850726f6a656374436f6e66696775726174696f6e506c6174666f726d7329203d20706f7374536f6c7574696f6e0d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e416374697665436667203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e4275696c642e30203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e416374697665436667203d2052656c656173657c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e4275696c642e30203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e50726f7065727469657329203d20707265536f6c7574696f6e0d0a090948696465536f6c7574696f6e4e6f6465203d2046414c53450d0a09456e64476c6f62616c53656374696f6e0d0a456e64476c6f62616c0d0a', NULL);
INSERT INTO versions_audit VALUES ('U', '2018-01-28 12:18:50.315428', 4, 1, '777', NULL, 2, '\xefbbbf0d0a4d6963726f736f66742056697375616c2053747564696f20536f6c7574696f6e2046696c652c20466f726d61742056657273696f6e2031322e30300d0a232056697375616c2053747564696f2031350d0a56697375616c53747564696f56657273696f6e203d2031352e302e32363430332e370d0a4d696e696d756d56697375616c53747564696f56657273696f6e203d2031302e302e34303231392e310d0a50726f6a65637428227b46414530344543302d333031462d313144332d424634422d3030433034463739454642437d2229203d2022536572766963654d6f64654f7470222c2022536572766963654d6f64654f74705c536572766963654d6f64654f74702e637370726f6a222c20227b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d220d0a456e6450726f6a6563740d0a476c6f62616c0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e436f6e66696775726174696f6e506c6174666f726d7329203d20707265536f6c7574696f6e0d0a090944656275677c416e7920435055203d2044656275677c416e79204350550d0a090952656c656173657c416e7920435055203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e2850726f6a656374436f6e66696775726174696f6e506c6174666f726d7329203d20706f7374536f6c7574696f6e0d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e416374697665436667203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e4275696c642e30203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e416374697665436667203d2052656c656173657c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e4275696c642e30203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e50726f7065727469657329203d20707265536f6c7574696f6e0d0a090948696465536f6c7574696f6e4e6f6465203d2046414c53450d0a09456e64476c6f62616c53656374696f6e0d0a456e64476c6f62616c0d0a', 'guest');
INSERT INTO versions_audit VALUES ('U', '2018-01-28 12:18:50.318384', 5, 1, '778', NULL, 1, '\xefbbbf0d0a4d6963726f736f66742056697375616c2053747564696f20536f6c7574696f6e2046696c652c20466f726d61742056657273696f6e2031322e30300d0a232056697375616c2053747564696f2031350d0a56697375616c53747564696f56657273696f6e203d2031352e302e32363430332e370d0a4d696e696d756d56697375616c53747564696f56657273696f6e203d2031302e302e34303231392e310d0a50726f6a65637428227b46414530344543302d333031462d313144332d424634422d3030433034463739454642437d2229203d2022536572766963654d6f64654f7470222c2022536572766963654d6f64654f74705c536572766963654d6f64654f74702e637370726f6a222c20227b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d220d0a456e6450726f6a6563740d0a476c6f62616c0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e436f6e66696775726174696f6e506c6174666f726d7329203d20707265536f6c7574696f6e0d0a090944656275677c416e7920435055203d2044656275677c416e79204350550d0a090952656c656173657c416e7920435055203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e2850726f6a656374436f6e66696775726174696f6e506c6174666f726d7329203d20706f7374536f6c7574696f6e0d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e416374697665436667203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e4275696c642e30203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e416374697665436667203d2052656c656173657c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e4275696c642e30203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e50726f7065727469657329203d20707265536f6c7574696f6e0d0a090948696465536f6c7574696f6e4e6f6465203d2046414c53450d0a09456e64476c6f62616c53656374696f6e0d0a456e64476c6f62616c0d0a', 'guest');
INSERT INTO versions_audit VALUES ('U', '2018-01-28 12:18:50.320197', 6, 1, '779', NULL, 3, '\xefbbbf0d0a4d6963726f736f66742056697375616c2053747564696f20536f6c7574696f6e2046696c652c20466f726d61742056657273696f6e2031322e30300d0a232056697375616c2053747564696f2031350d0a56697375616c53747564696f56657273696f6e203d2031352e302e32363430332e370d0a4d696e696d756d56697375616c53747564696f56657273696f6e203d2031302e302e34303231392e310d0a50726f6a65637428227b46414530344543302d333031462d313144332d424634422d3030433034463739454642437d2229203d2022536572766963654d6f64654f7470222c2022536572766963654d6f64654f74705c536572766963654d6f64654f74702e637370726f6a222c20227b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d220d0a456e6450726f6a6563740d0a476c6f62616c0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e436f6e66696775726174696f6e506c6174666f726d7329203d20707265536f6c7574696f6e0d0a090944656275677c416e7920435055203d2044656275677c416e79204350550d0a090952656c656173657c416e7920435055203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e2850726f6a656374436f6e66696775726174696f6e506c6174666f726d7329203d20706f7374536f6c7574696f6e0d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e416374697665436667203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e44656275677c416e79204350552e4275696c642e30203d2044656275677c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e416374697665436667203d2052656c656173657c416e79204350550d0a09097b37454238383838392d433138352d344145312d414230412d3138313038363536333030467d2e52656c656173657c416e79204350552e4275696c642e30203d2052656c656173657c416e79204350550d0a09456e64476c6f62616c53656374696f6e0d0a09476c6f62616c53656374696f6e28536f6c7574696f6e50726f7065727469657329203d20707265536f6c7574696f6e0d0a090948696465536f6c7574696f6e4e6f6465203d2046414c53450d0a09456e64476c6f62616c53656374696f6e0d0a456e64476c6f62616c0d0a', 'guest');


--
-- TOC entry 2376 (class 0 OID 0)
-- Dependencies: 190
-- Name: device_device_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('device_device_id_seq', 26, true);


--
-- TOC entry 2377 (class 0 OID 0)
-- Dependencies: 192
-- Name: function_function_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('function_function_id_seq', 21, true);


--
-- TOC entry 2378 (class 0 OID 0)
-- Dependencies: 194
-- Name: task_task_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('task_task_id_seq', 24, true);


--
-- TOC entry 2379 (class 0 OID 0)
-- Dependencies: 185
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('users_user_id_seq', 1, true);


--
-- TOC entry 2380 (class 0 OID 0)
-- Dependencies: 198
-- Name: version_version_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('version_version_id_seq', 9, true);


--
-- TOC entry 2174 (class 2606 OID 16461)
-- Name: device_groups device_group_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY device_groups
    ADD CONSTRAINT device_group_pkey PRIMARY KEY (group_id);


--
-- TOC entry 2178 (class 2606 OID 16477)
-- Name: devices device_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY devices
    ADD CONSTRAINT device_pkey PRIMARY KEY (device_id);


--
-- TOC entry 2176 (class 2606 OID 16466)
-- Name: device_status device_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY device_status
    ADD CONSTRAINT device_status_pkey PRIMARY KEY (status_id);


--
-- TOC entry 2180 (class 2606 OID 16631)
-- Name: devices device_uuid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY devices
    ADD CONSTRAINT device_uuid UNIQUE (uuid);


--
-- TOC entry 2182 (class 2606 OID 16536)
-- Name: functions function_function_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY functions
    ADD CONSTRAINT function_function_id PRIMARY KEY (function_id);


--
-- TOC entry 2194 (class 2606 OID 16689)
-- Name: task_host hask_host_task_id_host_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY task_host
    ADD CONSTRAINT hask_host_task_id_host_id PRIMARY KEY (task_id, host_id);


--
-- TOC entry 2172 (class 2606 OID 16456)
-- Name: rights rights_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rights
    ADD CONSTRAINT rights_pkey PRIMARY KEY (right_id);


--
-- TOC entry 2184 (class 2606 OID 16534)
-- Name: tasks task_task_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT task_task_id PRIMARY KEY (task_id);


--
-- TOC entry 2188 (class 2606 OID 16532)
-- Name: user_group user_groups_user_id_group_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_group
    ADD CONSTRAINT user_groups_user_id_group_id PRIMARY KEY (user_id, group_id);


--
-- TOC entry 2186 (class 2606 OID 16530)
-- Name: user_right user_rights_user_id_rights_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_right
    ADD CONSTRAINT user_rights_user_id_rights_id PRIMARY KEY (user_id, rights_id);


--
-- TOC entry 2166 (class 2606 OID 16451)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);


--
-- TOC entry 2168 (class 2606 OID 16629)
-- Name: users users_username; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_username UNIQUE (username);


--
-- TOC entry 2170 (class 2606 OID 16633)
-- Name: users users_uuid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_uuid UNIQUE (uuid);


--
-- TOC entry 2192 (class 2606 OID 16554)
-- Name: version_state version_state_version_state_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY version_state
    ADD CONSTRAINT version_state_version_state_id PRIMARY KEY (version_state_id);


--
-- TOC entry 2190 (class 2606 OID 16544)
-- Name: versions version_version_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY versions
    ADD CONSTRAINT version_version_id PRIMARY KEY (version_id);


--
-- TOC entry 2208 (class 2620 OID 16810)
-- Name: device_groups device_groups_ai; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER device_groups_ai AFTER INSERT OR DELETE OR UPDATE ON device_groups FOR EACH ROW EXECUTE PROCEDURE process_audit('device_groups_audit');


--
-- TOC entry 2209 (class 2620 OID 16811)
-- Name: device_status device_status_ai; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER device_status_ai AFTER INSERT OR DELETE OR UPDATE ON device_status FOR EACH ROW EXECUTE PROCEDURE process_audit('device_status_audit');


--
-- TOC entry 2210 (class 2620 OID 16812)
-- Name: devices devices_ai; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER devices_ai AFTER INSERT OR DELETE OR UPDATE ON devices FOR EACH ROW EXECUTE PROCEDURE process_audit('devices_audit');


--
-- TOC entry 2211 (class 2620 OID 16798)
-- Name: functions functions_audit; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER functions_audit AFTER INSERT OR DELETE OR UPDATE ON functions FOR EACH ROW EXECUTE PROCEDURE process_audit('functions_audit');


--
-- TOC entry 2207 (class 2620 OID 16813)
-- Name: rights rights_ai; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER rights_ai AFTER INSERT OR DELETE OR UPDATE ON rights FOR EACH ROW EXECUTE PROCEDURE process_audit('rights_audit');


--
-- TOC entry 2217 (class 2620 OID 16814)
-- Name: task_host task_host_ai; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER task_host_ai AFTER INSERT OR DELETE OR UPDATE ON task_host FOR EACH ROW EXECUTE PROCEDURE process_audit('task_host_audit');


--
-- TOC entry 2212 (class 2620 OID 16816)
-- Name: tasks tasks_ai; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER tasks_ai AFTER INSERT OR DELETE OR UPDATE ON tasks FOR EACH ROW EXECUTE PROCEDURE process_audit('tasks_audit');


--
-- TOC entry 2214 (class 2620 OID 16817)
-- Name: user_group user_group_ai; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER user_group_ai AFTER INSERT OR DELETE OR UPDATE ON user_group FOR EACH ROW EXECUTE PROCEDURE process_audit('user_group_audit');


--
-- TOC entry 2213 (class 2620 OID 16818)
-- Name: user_right user_right_ai; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER user_right_ai AFTER INSERT OR DELETE OR UPDATE ON user_right FOR EACH ROW EXECUTE PROCEDURE process_audit('user_right_audit');


--
-- TOC entry 2206 (class 2620 OID 16819)
-- Name: users users_ai; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER users_ai AFTER INSERT OR DELETE OR UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE process_audit('users_audit');


--
-- TOC entry 2216 (class 2620 OID 16820)
-- Name: version_state version_state_ai; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER version_state_ai AFTER INSERT OR DELETE OR UPDATE ON version_state FOR EACH ROW EXECUTE PROCEDURE process_audit('version_state_audit');


--
-- TOC entry 2215 (class 2620 OID 16821)
-- Name: versions versions_ai; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER versions_ai AFTER INSERT OR DELETE OR UPDATE ON versions FOR EACH ROW EXECUTE PROCEDURE process_audit('versions_audit');


--
-- TOC entry 2195 (class 2606 OID 16478)
-- Name: devices device_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY devices
    ADD CONSTRAINT device_group_id_fkey FOREIGN KEY (group_id) REFERENCES device_groups(group_id);


--
-- TOC entry 2196 (class 2606 OID 16483)
-- Name: devices device_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY devices
    ADD CONSTRAINT device_status_id_fkey FOREIGN KEY (status_id) REFERENCES device_status(status_id);


--
-- TOC entry 2203 (class 2606 OID 16690)
-- Name: task_host hask_host_device_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY task_host
    ADD CONSTRAINT hask_host_device_id_fkey FOREIGN KEY (device_id) REFERENCES devices(device_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2204 (class 2606 OID 16695)
-- Name: task_host hask_host_function_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY task_host
    ADD CONSTRAINT hask_host_function_id_fkey FOREIGN KEY (function_id) REFERENCES functions(function_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2205 (class 2606 OID 16700)
-- Name: task_host hask_host_version_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY task_host
    ADD CONSTRAINT hask_host_version_id_fkey FOREIGN KEY (version_id) REFERENCES versions(version_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2200 (class 2606 OID 16524)
-- Name: user_group user_groups_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_group
    ADD CONSTRAINT user_groups_group_id_fkey FOREIGN KEY (group_id) REFERENCES device_groups(group_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2199 (class 2606 OID 16519)
-- Name: user_group user_groups_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_group
    ADD CONSTRAINT user_groups_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2198 (class 2606 OID 16514)
-- Name: user_right user_rights_rights_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_right
    ADD CONSTRAINT user_rights_rights_id_fkey FOREIGN KEY (rights_id) REFERENCES rights(right_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2197 (class 2606 OID 16509)
-- Name: user_right user_rights_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_right
    ADD CONSTRAINT user_rights_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2201 (class 2606 OID 16545)
-- Name: versions version_function_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY versions
    ADD CONSTRAINT version_function_id_fkey FOREIGN KEY (function_id) REFERENCES functions(function_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2202 (class 2606 OID 16555)
-- Name: versions version_version_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY versions
    ADD CONSTRAINT version_version_state_id_fkey FOREIGN KEY (version_state_id) REFERENCES version_state(version_state_id) ON UPDATE CASCADE ON DELETE RESTRICT;


-- Completed on 2018-02-09 16:41:40

--
-- PostgreSQL database dump complete
--

