﻿module.exports.getName = function(description) {
    if (description) {
        var firstLine = description.split('\n')[0];
        return firstLine.split(/\s+/).slice(0, 10).join(" ");
    }
    return description;
}

module.exports.isEqualVariableTypes = function(variable1, variable2) {
    if (variable1 && variable2) {
        if (variable1.type != variable2.type) return false;
        // check enum type
        if (variable1.type == 4) {
            if (!(Array.isArray(variable1.enumValues) && Array.isArray(variable2.enumValues))) return false;
            if (variable1.enumValues.length != variable3.enumValues.length) return false;
            for (var i = 0; i < variable1.length; i++) {
                if (variable2.enumValues.indexOf(variable1.enumValues[i]) == -1) return false;
            }
            return true;
        } else {
            return true;
        }
    }
    return false;
}