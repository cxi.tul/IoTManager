﻿var React = require('react');
var createReactClass = require('create-react-class');
import { Icon, Label, Form, Button } from 'semantic-ui-react'
var validator = require('validator');
import { tr } from './translation';

var VariableValue = createReactClass({
    removeValue() {
        if (this.props.removeValue) this.props.removeValue(this.props.value);
    },

    render: function () {
        return (
            <Label>
                {this.props.value} <i onClick={this.removeValue} className="fa fa-times" aria-hidden="true"></i>
            </Label>);
    }
})

var FunctionVariableInfo = createReactClass({

    variableTypes: [{ key: 0, value: 0, text: "Boolean" }, { key: 1, value: 1, text: "Integer" }, { key: 2, value: 2, text: "Float" }, { key: 3, value: 3, text: "String" }, { key: 4, value: 4, text: "Enum" }],

    getInitialState: function () {
        return { newValue: "" };
    },

    variableIdChanged(event, { value }) {
        this.props.variable.keyEdit = value;
        if (this.props.variableIdChanged) this.props.variableIdChanged(this.props.variable);
    },

    variableDescriptionChanged(event, { value }) {
        this.props.variable.description = value;
        if (this.props.variableChanged) this.props.variableChanged(this.props.variable);
    },

    variableTypeChanged(event, { value }){
        this.props.variable.type = value;
        if (this.props.variableChanged) this.props.variableChanged(this.props.variable);
    },

    variableDefaultValueChanged(event, { value }) {
        this.props.variable.defaultValue = value;
        if (this.props.variableChanged) this.props.variableChanged(this.props.variable);
    },

    variableDefaultBoolValueChanged(event, object) {
        this.props.variable.defaultValue = object.checked;
        if (this.props.variableChanged) this.props.variableChanged(this.props.variable);
    },

    newValueChanged(event, { value }) {
        this.setState({ newValue: value });
    },

    addNewValue() {
        if (this.state.newValue) {
            if (this.props.variable.enumValues) {
                if (this.props.variable.enumValues.indexOf(this.state.newValue) == -1) {
                    this.props.variable.enumValues = this.props.variable.enumValues.concat([this.state.newValue]);
                } else {
                    this.setState({ newValue: "" });
                    return;
                }
            } else {
                this.props.variable.enumValues = [this.state.newValue];
            }
            this.setState({ newValue: "" });
            if (this.props.variableChanged) this.props.variableChanged(this.props.variable);
        }
    },

    removeValue(value) {
        if (this.props.variable.enumValues) {
            var position = this.props.variable.enumValues.indexOf(value);
            if (position != -1) {
                this.props.variable.enumValues.splice(position, 1);
                if (this.props.variableChanged) this.props.variableChanged(this.props.variable);
            }
        }
    },

    isValid(type, stringValue, enumValues) {
        switch (type) {
            case 1: return validator.isInt(String(stringValue), { locale: globalLanguageCountry });
            case 2: return validator.isFloat(String(stringValue), { locale: globalLanguageCountry });
            case 4: return enumValues && enumValues.indexOf(String(stringValue)) != -1;
            default: return true;
        }
    },

    render: function () {
        var variableType = this.props.variable.type === undefined ? 0 : this.props.variable.type;

        var enumValues = this.props.variable.enumValues ? this.props.variable.enumValues.map((value, index) => { return { key: index, value: value, text: value }; }) : null;
        var enumValueComponents = this.props.variable.enumValues ? this.props.variable.enumValues.map((value) =>
            <VariableValue key={value} value={value} removeValue={this.removeValue} />
        ) : null;
        var keyError = this.props.variable.key != this.props.variable.keyEdit;
        var validationError = !this.isValid(variableType, this.props.variable.defaultValue, this.props.variable.enumValues);

        var defaultValueInput = null;
        switch (variableType) {
            case 0:
                defaultValueInput = <Form.Checkbox toggle checked={this.props.variable.defaultValue ? true : false} label={tr("Default value")} onChange={this.variableDefaultBoolValueChanged} />
                break;
            case 4:
                defaultValueInput = <Form.Select error={validationError} options={enumValues} value={this.props.variable.defaultValue === undefined ? null : this.props.variable.defaultValue} label={tr("Default value")} onChange={this.variableDefaultValueChanged} />;
                break;
            default:
                defaultValueInput = <Form.Input error={validationError} value={this.props.variable.defaultValue === null ? undefined : this.props.variable.defaultValue} label={tr("Default value")} onChange={this.variableDefaultValueChanged} />;
                break;
        }
        return (
            <Form>
                <Form.Input error={keyError} value={this.props.variable.keyEdit ? this.props.variable.keyEdit : ''} label={tr("Variable name")} onChange={this.variableIdChanged} />
                <Form.Input value={this.props.variable.description ? this.props.variable.description : ''} label={tr("Description")} onChange={this.variableDescriptionChanged} />
                <Form.Select options={this.variableTypes} value={variableType === undefined ? 1 : variableType} label={tr("Type")} onChange={this.variableTypeChanged} />

                {(variableType == 4) ?
                    <div>
                        {defaultValueInput}
                        {enumValueComponents}
                        <Form.Input label={tr("New value")} value={this.state.newValue} onChange={this.newValueChanged} />
                        <Button onClick={this.addNewValue}>{tr("Add value")}</Button>
                    </div>
                    :
                    <div>
                        {defaultValueInput}
                    </div>
                }
            </Form>
        );
    }
});

module.exports = FunctionVariableInfo;
