﻿var React = require('react');
var createReactClass = require('create-react-class');
import { Button, Checkbox, Icon, Table, Input } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import { tr } from './translation';

var TaskFunctionSelector = createReactClass({

    getInitialState: function () {
        return { text: '', lines: 5 };
    },

    toggleShowAll() {
        this.setState({ lines: (this.state.lines < 0) ? 5 : -1 });
    },

    filterByText(event, { value }) {
        this.setState({ text: value });
    },

    functionSelected(fce) {
        if (this.props.onSelected) this.props.onSelected(fce);
    },

    render: function () {
        var lines = 0;
        var rows = this.props.functions.filter(function (fce) {
            if (this.state.text) {
                if (!(fce.description && fce.description.toLowerCase().search(this.state.text) != -1)) {
                    return false;
                }
            }

            lines++;
            return (this.state.lines >= 0 && lines > this.state.lines) ? false : true;

        }, this)
            .map((fce, index) => {
                var active = this.props.function && fce.id == this.props.function.id;
                return (
                    <Table.Row active={active} key={fce.id} className="cursor-pointer" onClick={this.functionSelected.bind(this, fce)}>
                        <Table.Cell>{fce.description &&
                            <span className="description"> {fce.description} </span>
                        }</Table.Cell>
                    </Table.Row>
                );
            });

        return (
            <div>
                <Input icon='search' value={this.state.text} onChange={this.filterByText} />
                <Table striped>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Popis</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {rows}
                    </Table.Body>
                </Table>
                <div className="footer">
                    <Button onClick={this.toggleShowAll}>
                        {(this.state.lines < 0) ? tr("Show") + " 5" : tr("Show all")}
                    </Button>
                </div>
            </div>
        );
    }
});

module.exports = TaskFunctionSelector;
