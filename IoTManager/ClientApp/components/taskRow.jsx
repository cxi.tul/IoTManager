﻿var React = require('react');
var createReactClass = require('create-react-class');
import { Button, Checkbox, Icon, Table } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import { tr } from './translation';

var TaskRow = createReactClass({

    handleCheckedChange: function (e) {
        //e.preventDefault();
        if (this.props.checkedChanged) this.props.checkedChanged(this.props.task.id);
    },

    handleActiveChange: function (e) {
        //e.preventDefault();
        this.props.task.active = !this.props.task.active;
        if (this.props.activeChanged) this.props.activeChanged(this.props.task);
    },

    render: function () {
        var taskLink = ((this.props.match) ? this.props.match.url : '') + '/task/' + this.props.task.id;
        return (
            <Table.Body>
                <Table.Row>
                    <Table.Cell collapsing>
                        <Checkbox checked={this.props.checked} onChange={this.handleCheckedChange} />
                    </Table.Cell>
                    <Table.Cell collapsing>
                        <Checkbox toggle checked={this.props.task.active} onChange={this.handleActiveChange} />
                    </Table.Cell>
                    <Table.Cell>
                        <NavLink to={{ pathname: taskLink }} >
                        {this.props.task.name &&
                            <span className="description"> {this.props.task.name} </span>
                        }
                        </NavLink>
                    </Table.Cell>
                    <Table.Cell collapsing>{this.props.task.update &&
                        <i className="fa fa-refresh update" aria-hidden="true"></i>
                    }</Table.Cell>
                    <Table.Cell collapsing><NavLink to={{ pathname: taskLink }} ><Button basic className="green">{tr("Detail")}</Button></NavLink></Table.Cell>
                </Table.Row>
            </Table.Body>
        );
    }
});

module.exports = TaskRow;
