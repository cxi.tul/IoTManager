﻿var React = require('react');
var createReactClass = require('create-react-class');
var AggregatedCounts = require('./components/aggregatedCounts');
var DropDownMenu = require('./components/dropDownMenu');
var SimpleMenuItem = require('./components/simpleMenuItem');
var FunctionVariableInfo = require('./components/functionVariableInfo');
var VersionInfo = require('./components/versionInfo');
import { Icon, Menu, Grid, Segment, TextArea, Select, Input, Form, Button, Table } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import { getName } from './components/utils'
import { hasRight } from './components/userRights'
import { tr } from './components/translation';

var FunctionDetail = createReactClass({
    
    functionFormats: [
        { key: 0, value: null, text: "" },
        { key: 1, value: "JS", text: "JS" },
        { key: 2, value: "C#", text: "C#" },
        { key: 3, value: "JAVA", text: "JAVA" },
        { key: 4, value: "SQL", text: "SQL" },
        { key: 4, value: "MQ", text: "MQ" },
        { key: 4, value: "EXE", text: "EXE" },
        { key: 4, value: "BAT", text: "BAT" },
    ],

    getInitialState: function () {
        return { function: { inputs: {}, inputsList: [], outputs: {}, outputsList: [], parameters: {}, parametersList: [] }, timeoutId: null, changed: false, versionsVisible: false };
    },

    componentDidMount: function () {
        this.loadFunction();
    },

    toggleVersionsVisibility() {
        this.setState({ versionsVisible: !this.state.versionsVisible });
    },

    createVariablesList(variablesObject) {
        var result = [];
        for (var key in variablesObject) {
            var variable = variablesObject[key];
            variable.key = key;
            result.push(variable);
        }
        return result;
    },

    updateFunction(fce) {
        if (fce) {
            if (!fce.inputs) fce.inputs = {};
            fce.inputsList = this.createVariablesList(fce.inputs);
            if (!fce.outputs) fce.outputs = {};
            fce.outputsList = this.createVariablesList(fce.outputs);
            if (!fce.parameters) fce.parameters = {};
            fce.parametersList = this.createVariablesList(fce.parameters);
            if (!fce.versions) fce.versions = [];
            fce.displayName = fce.name;
            this.setState({ function: fce, changed: false });
        }
    },

    loadFunction() {
        if (!this.props.match || this.props.match.params.id === undefined || this.props.match.params.id == 'new') {
            this.updateFunction({});
        } else {
            $.ajax({
                url: "function.detail",
                cache: false,
                context: this,
                data: { id: this.props.match.params.id }
            })
                .done(function (fce) {
                    this.updateFunction(fce);
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status == 401) window.location = "home.login";
                })
                .always(function () {
                });
        }
    },

    saveFunction() {

        var fd = new FormData();

        this.state.function.versions.filter((version) => {
            return !version.stored && version.file;
        })
            .forEach((version) => {
                fd.append('files', version.file);
            });

        fd.append('functionJson', JSON.stringify(this.state.function));

        $.ajax({
            url: "function.edit",
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
        })
            .done(function (fce) {
                this.updateFunction(fce);
                var link = document.body.querySelector("h2.backlink a");
                if (link) {
                    link.click();
                }
            }.bind(this))
            .fail(function () {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
            });
    },

    changeFunctionLocal(event, { value }) {
        if (!hasRight('developer')) return;
        this.state.function.local = !this.state.function.local;
        this.setState({ function: this.state.function, changed: true });
    },

    changeFunctionName(event, { value }) {
        if (!hasRight('developer')) return;
        this.state.function.displayName = value;
        var re = /\W/i;
        if (!this.state.function.displayName.match(re)) {
            this.state.function.name = value;
        }
        this.setState({ function: this.state.function, changed: true });
    },

    changeFunctionFormat(event, { value }) {
        if (!hasRight('developer')) return;
        this.state.function.format = value;
        this.setState({ function: this.state.function, changed: true });
    },

    changeFunctionDescription(event, { value }) {
        if (!hasRight('developer')) return;
        this.state.function.description = value;
        this.setState({ function: this.state.function, changed: true });
    },

    indicateVersionsChange() {
        this.setState({ function: this.state.function, changed: true });
    },

    addVersion(event) {
        var newVersion = { stateId: 1 };

        if (event.target.files && event.target.files.length > 0) {
            newVersion.file = event.target.files[0];
        }
        event.target.value = null;

        var fce = this.state.function;
        if (fce.versions && fce.versions.length > 0) {
            if (fce.versions[fce.versions.length - 1].number) {
                var parts = fce.versions[fce.versions.length - 1].number.split(".");
                parts[parts.length - 1] = parseInt(parts[parts.length - 1]) + 1;
                newVersion.number = parts.join(".");
            } else newVersion.number = "1";
        } else {
            newVersion.number = "1";
        }
        fce.versions = fce.versions.concat([newVersion]);
        this.setState({ function: fce, changed: true });
    },

    indicateVariableChange() {
        this.setState({ function: this.state.function, changed: true });
    },

    approveVariableIdChange(variable, variablesObject) {
        if (Object.keys(variablesObject).indexOf(variable.keyEdit) == -1){
            variablesObject[variable.keyEdit] = variable;
            delete variablesObject[variable.key];
            variable.key = variable.keyEdit
        }
        this.setState({ function: this.state.function, changed: true });
    },

    prepareNewVariable(variablesObject) {
        var variable = { key: 'variable' };
        if (variablesObject) {
            var count = 1;
            while (Object.keys(variablesObject).indexOf(variable.key) != -1) {
                variable.key = 'variable' + count;
                count++;
            }
        }
        return variable;
    },

    addVariable(variablesObject, variablesList) {
        var variable = this.prepareNewVariable(variablesObject);
        variablesObject[variable.key] = variable;
        variablesList.push(variable);
        this.setState({ function: this.state.function, changed: true });
    },

    removeVariable(variable, variablesObject, variablesList) {
        delete variablesObject[variable.key];
        var index = -1;
        for (var i = 0; i < variablesList.length; i++) {
            if (variable.key == variablesList[i].key) {
                variablesList.splice(i, 1);    
                break;
            }
        }
        this.setState({ function: this.state.function, changed: true });
    },

    createVariables(valiablesList, variablesObject) {
        return valiablesList.map((variable, index) => {
            if (!variable.keyEdit) variable.keyEdit = variable.key;

            if (hasRight('developer')) {
                return (<Segment key={index}>
                    <div className="content-right">
                        <i onClick={this.removeVariable.bind(this, variable, variablesObject, valiablesList)} className="transparent-button fa fa-trash" aria-hidden="true"></i>
                    </div>
                    <FunctionVariableInfo variable={variable} variableChanged={this.indicateVariableChange} variableIdChanged={this.approveVariableIdChange.bind(this, variable, variablesObject)} />
                </Segment >);
            } else {
                return (<div>{variable.description ? getName(variable.description) : variable.key}</div >);
            }
        });
    },

    render: function () {

        var inputs = this.createVariables(this.state.function.inputsList, this.state.function.inputs);
        var parameters = this.createVariables(this.state.function.parametersList, this.state.function.parameters);
        var outputs = this.createVariables(this.state.function.outputsList, this.state.function.outputs);

        var versionStates = Object.keys(userData.AllVersionStates).map((key) => { return { key: key, value: parseInt(key), text: userData.AllVersionStates[key] }; });

        var versions = this.state.versionsVisible && this.state.function.versions ?
            this.state.function.versions.map((version, index) =>
                <VersionInfo key={index} version={version} functionId={this.state.function.id} versionChanged={this.indicateVersionsChange} versionStates={versionStates} />
            ) : null;

        var saveButton = (this.state.changed) ? <Button color="green" onClick={this.saveFunction}>{tr("Save")}</Button> : <Button onClick={this.saveFunction}>{tr("Save")}</Button>;

        return (
            <div>
                <header className="main-header">
                    <div className="main-column">
                        <h2 className="backlink"><NavLink to={{ pathname: '/function' }}><i className="fa fa-long-arrow-left" aria-hidden="true"></i> {tr("Functions list")}</NavLink></h2>
                        <div className="left-header">
                            <h1>{tr("Function settings")}</h1>
                            <div className="menu-buttons green">
                                {hasRight('administrator') && saveButton}
                            </div>                            
                        </div>
                        <div className="list-item-detail">
                            <div className="id"><span>{getName(this.state.function.description)}</span></div>
                        </div>
                    </div>
                </header>
                <div className="container body-content main-column">

                        <Form>
                        <Form.Checkbox toggle checked={this.state.function.local} onChange={this.changeFunctionLocal} label={tr("Local installation possible")} />
                        <Form.TextArea value={this.state.function.description ? this.state.function.description : ''} label={tr("Description")} onChange={this.changeFunctionDescription} />
                        <Form.Input error={this.state.function.displayName != this.state.function.name} value={this.state.function.displayName ? this.state.function.displayName : ''} label={tr("Name")} onChange={this.changeFunctionName} />
                        <Form.Select options={this.functionFormats} value={this.state.function.format === undefined ? null : this.state.function.format} label={tr("Format")} onChange={this.changeFunctionFormat} />
                        </Form>

                        {hasRight('developer') &&
                            <Table celled striped>
                                <Table.Header>
                                    <Table.Row>
                                <Table.HeaderCell colSpan='4' onClick={this.toggleVersionsVisibility}>{tr("Versions")} <i className="fa fa-caret-down" aria-hidden="true"></i></Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                {this.state.versionsVisible &&
                                    <Table.Header>
                                        <Table.Row>
                                <Table.HeaderCell>{tr("Name")}</Table.HeaderCell>
                                <Table.HeaderCell>{tr("Type")}</Table.HeaderCell>
                                <Table.HeaderCell colSpan="2">{tr("Description")}</Table.HeaderCell>
                                        </Table.Row>
                                    </Table.Header>
                                }
                                {this.state.versionsVisible &&
                                    <Table.Body>
                                        {versions}
                                    </Table.Body>
                                }
                                {this.state.versionsVisible &&
                                    <Table.Footer>
                                        <Table.Row>
                                            <Table.Cell colSpan='3'>
                                    <label className="ui button left floated">{tr("Add")}<input type="file" onChange={this.addVersion} /></label>
                                            </Table.Cell>
                                        </Table.Row>
                                    </Table.Footer>
                                }
                            </Table>
                        }

                    <Grid columns='equal'>
                        <Grid.Column>
                            <Segment>
                                <h4>{tr("Inputs")}</h4>
                                {inputs}
                                {hasRight('developer') && <Button onClick={this.addVariable.bind(this, this.state.function.inputs, this.state.function.inputsList)}>{tr("Add input")}</Button>}
                            </Segment>
                        </Grid.Column>
                        <Grid.Column>
                            <Segment>
                                <h4>{tr("Parameters")}</h4>
                                {parameters}
                                {hasRight('developer') && <Button onClick={this.addVariable.bind(this, this.state.function.parameters, this.state.function.parametersList)}>{tr("Add parameter")}</Button>}
                            </Segment>
                        </Grid.Column>
                        <Grid.Column>
                            <Segment>
                                    <h4>{tr("Outputs")}</h4>
                                {outputs}
                                {hasRight('developer') && <Button onClick={this.addVariable.bind(this, this.state.function.outputs, this.state.function.outputsList)}>{tr("Add output")}</Button>}
                            </Segment>
                        </Grid.Column>
                    </Grid>

                </div>
            </div>
        );
    }
});

module.exports = FunctionDetail;
