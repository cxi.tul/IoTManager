﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoTManager.Models
{
    //public class DeviceGroupModel
    //{
    //    public long? Id { get; set; }
    //    public string Name { get; set; }
    //}

    public class DeviceModel: StoredModel
    {
        public long Id { get; set; }
        public string GroupName { get; set; }
        public long? GroupId { get; set; }
        public string Description { get; set; }
        public bool? Online { get; set; }
        public bool Updates { get; set; }
        public string Version { get; set; }
        public DateTime? LastOnline { get; set; }
        public string Ip { get; set; }
        public string FirmwareVersion { get; set; }
        public Guid? Uuid { get; set; }
    }

    public class DeviceModelDetail : DeviceModel
    {
        public List<TaskModel> Tasks { get; set; }

        public static DeviceModelDetail FromDeviceModel(DeviceModel source, List<TaskModel> Tasks)
        {
            string serializedSource = JsonConvert.SerializeObject(source);
            var result = JsonConvert.DeserializeObject<DeviceModelDetail>(serializedSource);
            result.Tasks = Tasks;
            return result;
        }
    }
}
