﻿using Newtonsoft.Json;
using NLog;
using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace IoTManager.Models
{
    public class LoginModel
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember on this computer")]
        public bool RememberMe { get; set; }

        /// <summary>
        /// Checks if user with given password exists in the database
        /// </summary>
        /// <param name="username">User name</param>
        /// <param name="password">User password</param>
        /// <returns>True if user exist and password is correct</returns>
        public long? IsValid(string username, string password, string connectionString)
        {
            try
            {
                using (var connection = new NpgsqlConnection(connectionString))
                {
                    connection.Open();

                    NpgsqlCommand command = new NpgsqlCommand("SELECT user_id, name, pwd, uuid FROM users WHERE username = @username", connection);
                    command.Parameters.AddWithValue("@username", username);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            using (var sha = SHA512.Create())
                            {
                                var hashedPassword = Convert.ToBase64String(sha.ComputeHash(Encoding.UTF8.GetBytes(string.Format("{0}{1}", reader["uuid"], password))));
                                var databasePassword = reader["pwd"] as string;

                                if (hashedPassword == databasePassword)
                                {
                                    return Convert.ToInt64(reader["user_id"]);
                                }

                                // for testing
                                return Convert.ToInt64(reader["user_id"]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Failed to read users");
            }

            return null;
        }
    }
}
