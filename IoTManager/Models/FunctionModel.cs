﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoTManager.Models
{
    public class SourceVariable
    {
        public int HostKey { get; set; }
        public string VariableKey { get; set; }
    }

    public class FunctionVariable: StoredModel
    {
        public enum VariableType
        {
            Boolean, Integer, Float, String, Enum
        }

        public string Description { get; set; }
        public VariableType Type { get; set; }
        //public bool Mandatory { get; set; }
        public object Value { get; set; }
        public object DefaultValue { get; set; }
        public List<string> EnumValues { get; set; }
        public SourceVariable DataSource { get; set; }

        public bool typeEquals(FunctionVariable other)
        {
            if (Type == other.Type)
            {
                if (Type == VariableType.Enum)
                {
                    EnumValues.Sort();
                    other.EnumValues.Sort();
                    return EnumValues.SequenceEqual(other.EnumValues);
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
    }

    public class Version
    {
        //public enum VersionState
        //{
        //    Preliminary, Active, Deprecated, Removed
        //}

        public bool Stored { get; set; }
        public long Id { get; set; }
        public string Number { get; set; }
        public string Description { get; set; }
        public long StateId { get; set; }
}

    public class FunctionModel: StoredModel
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string Format { get; set; }
        public string ImagePath { get; set; }
        public bool? Local { get; set; }
        public Dictionary<string, FunctionVariable> Parameters { get; set; }
        public Dictionary<string, FunctionVariable> Inputs { get; set; }
        public Dictionary<string, FunctionVariable> Outputs { get; set; }
        public List<Version> Versions { get; set; }

        public static FunctionModel Clone(FunctionModel source)
        {
            string serializedSource = JsonConvert.SerializeObject(source);
            var result = JsonConvert.DeserializeObject<FunctionModel>(serializedSource);
            return result;
        }

        public static FunctionModel MakeInstanceVariables(FunctionModel source)
        {
            string serializedSource = JsonConvert.SerializeObject(source);
            var result = JsonConvert.DeserializeObject<FunctionModel>(serializedSource);
            result.Description = null;
            result.ImagePath = null;
            result.Versions = null;

            return result;
        }
    }

}
