﻿using IoTManager.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Transactions;

namespace IoTManager.Controllers {
    public class DeviceController : BaseController
    {
        private readonly ILogger<DeviceController> logger;

        public DeviceController(IHostingEnvironment env, IOptions<AppSettings> appSettings, ILogger<DeviceController> logger) : base(env, appSettings, logger) {
            this.logger = logger;
        }

        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult List()
        {
            var userId = GetUserId();
            if (!userId.HasValue) return Unauthorized();

            return Json(GetDevices(userId.Value));
        }

        private List<DeviceModel> GetDevices(long userId)
        {
            var devices = new List<DeviceModel>();

            try
            {
                using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString))
                {
                    connection.Open();

                    NpgsqlCommand command = new NpgsqlCommand("SELECT devices.device_id, devices.description, device_groups.name, devices.online, devices.ts_last_online FROM devices LEFT JOIN device_groups ON devices.group_id = device_groups.group_id WHERE devices.group_id IS NULL OR devices.group_id IN (SELECT user_group.group_id FROM user_group WHERE user_group.user_id = @user_id) ORDER BY devices.device_id", connection);
                    command.Parameters.AddWithValue("@user_id", userId);
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            devices.Add(new DeviceModel()
                            {
                                Id = Convert.ToInt64(reader["device_id"]),
                                Description = reader["description"] as string,
                                GroupName = reader["name"] as string,
                                Online = reader["online"] as bool?,
                                LastOnline = reader["ts_last_online"] as DateTime?,
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to read devices");
            }

            return devices;
        }

        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Add()
        {
            var userId = GetUserId();
            if (!userId.HasValue) return Unauthorized();
            if (!hasRight(userId.Value, administratorRight)) return Unauthorized();

            try
            {
                using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString))
                {
                    connection.Open();

                    NpgsqlCommand command = new NpgsqlCommand("INSERT INTO devices (description, uuid, modified_by) VALUES (@description, @uuid, (SELECT users.username FROM users WHERE users.user_id = @user_id))", connection);
                    command.Parameters.AddWithValue("@description", "Nove zarizeni");
                    command.Parameters.AddWithValue("@uuid", Guid.NewGuid());
                    command.Parameters.AddWithValue("@user_id", userId);
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to read devices");
            }

            return Json(GetDevices(userId.Value));
        }

        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Delete(long id)
        {

            var userId = GetUserId();
            if (!userId.HasValue) return Unauthorized();
            if (!hasRight(userId.Value, administratorRight)) return Unauthorized();

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString))
                    {
                        connection.Open();
                        {
                            NpgsqlCommand command = new NpgsqlCommand("UPDATE devices SET modified_by = (SELECT users.username FROM users WHERE users.user_id = @user_id) WHERE device_id = @device_id AND (devices.group_id IS NULL OR devices.group_id IN (SELECT user_group.group_id FROM user_group WHERE user_group.user_id = @user_id))", connection);
                            command.Parameters.AddWithValue("@user_id", userId);
                            command.Parameters.AddWithValue("@device_id", id);
                            command.ExecuteNonQuery();
                        }
                        {
                            NpgsqlCommand command = new NpgsqlCommand("DELETE FROM devices WHERE device_id = @device_id AND (devices.group_id IS NULL OR devices.group_id IN (SELECT user_group.group_id FROM user_group WHERE user_group.user_id = @user_id))", connection);
                            command.Parameters.AddWithValue("@device_id", id);
                            command.Parameters.AddWithValue("@user_id", userId);
                            command.ExecuteNonQuery();
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to read devices");
            }

            return Json(GetDevices(userId.Value));
        }

        private DeviceModelDetail GetDevice(long id, long userId)
        {
            DeviceModelDetail device = null;
            try
            {
                using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString))
                {
                    connection.Open();

                    {
                        NpgsqlCommand command = new NpgsqlCommand("SELECT devices.device_id, devices.description, devices.online, devices.ts_last_online, devices.ip, devices.firmware_version, devices.uuid, device_groups.group_id FROM devices LEFT JOIN device_groups ON devices.group_id = device_groups.group_id WHERE devices.device_id = @device_id AND (devices.group_id IS NULL OR devices.group_id IN (SELECT user_group.group_id FROM user_group WHERE user_group.user_id = @user_id))", connection);
                        command.Parameters.AddWithValue("@device_id", id);
                        command.Parameters.AddWithValue("@user_id", userId);
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                device = new DeviceModelDetail()
                                {
                                    Stored = true,
                                    Id = Convert.ToInt64(reader["device_id"]),
                                    Description = reader["description"] as string,
                                    GroupId = reader["group_id"] as int?,
                                    Online = reader["online"] as bool?,
                                    LastOnline = reader["ts_last_online"] as DateTime?,
                                    Ip = reader["ip"] as string,
                                    FirmwareVersion = reader["firmware_version"] as string,
                                    Uuid = reader["uuid"] as Guid?,
                                };
                            }
                        }
                    }

                    if (device != null)
                    {
                        device.Tasks = new List<TaskModel>();
                        NpgsqlCommand command = new NpgsqlCommand("SELECT DISTINCT ON (tasks.task_id) tasks.task_id, tasks.description, tasks.name, tasks.active FROM tasks, task_host WHERE tasks.task_id = task_host.task_id AND task_host.device_id = @device_id", connection);
                        command.Parameters.AddWithValue("@device_id", id);
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                device.Tasks.Add(new TaskModel()
                                {
                                    Id = Convert.ToInt64(reader["task_id"]),
                                    Description = reader["description"] as string,
                                    Name = reader["name"] as string,
                                    Active = reader["active"] as bool?,
                                });
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to read device");
            }

            return device;
        }

        public IActionResult Detail(long id)
        {
            var userId = GetUserId();
            if (!userId.HasValue) return Unauthorized();

            return Json(GetDevice(id, userId.Value));
        }

        [HttpPost]
        public IActionResult Edit([FromBody]DeviceModel device)
        {
            var userId = GetUserId();
            if (!userId.HasValue) return Unauthorized();
            if (!hasRight(userId.Value, administratorRight)) return Unauthorized();

            try
            {
                using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString))
                {
                    connection.Open();

                    {
                        NpgsqlCommand command = new NpgsqlCommand("UPDATE devices SET description = @description, group_id = @group_id, modified_by = (SELECT users.username FROM users WHERE users.user_id = @user_id) WHERE device_id = @device_id AND (devices.group_id IS NULL OR devices.group_id IN (SELECT user_group.group_id FROM user_group WHERE user_group.user_id = @user_id))", connection);
                        command.Parameters.AddWithValue("@user_id", userId);
                        command.Parameters.AddWithValue("@device_id", device.Id);
                        command.Parameters.AddWithValue("@description", device.Description);
                        if (device.GroupId.HasValue) command.Parameters.AddWithValue("@group_id", device.GroupId);
                        else command.Parameters.AddWithValue("@group_id", DBNull.Value);

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to read device");
            }

            return Json(GetDevice(device.Id, userId.Value));

        }
    }
}