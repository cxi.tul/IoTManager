﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IoTManager.Models;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.Options;
using Npgsql;
using NLog;
using Microsoft.Extensions.Logging;
using System.Transactions;
using System.Text.RegularExpressions;

namespace IoTManager.Controllers
{
    public class FunctionController : BaseController
    {
        private readonly ILogger<FunctionController> logger;

        public FunctionController(IHostingEnvironment env, IOptions<AppSettings> appSettings, ILogger<FunctionController> logger) : base(env, appSettings,logger) {
            this.logger = logger;
        }

        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult List()
        {
            var userId = GetUserId();
            if (!userId.HasValue) return Unauthorized();

            return Json(GetFunctions(userId.Value));

        }

        private List<FunctionModel> GetFunctions(long userId)
        {
            var functions = new List<FunctionModel>();

            try
            {
                using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString))
                {
                    connection.Open();

                    //NpgsqlCommand command = new NpgsqlCommand(
                    //    (variables) ?
                    //    "SELECT functions.function_id, functions.description, functions.local, functions.parameters, functions.inputs, functions.outputs FROM functions" :
                    //    "SELECT functions.function_id, functions.description, functions.local FROM functions"
                    //    , connection);
                    NpgsqlCommand command = new NpgsqlCommand("SELECT functions.function_id, functions.description, functions.local, functions.parameters, functions.inputs, functions.outputs FROM functions ORDER BY functions.function_id", connection);
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var function = new FunctionModel()
                            {
                                Id = Convert.ToInt64(reader["function_id"]),
                                Description = reader["description"] as string,
                                Local = reader["local"] as bool?,
                            };

                            //if (variables)
                            {
                                function.Parameters = ((reader["parameters"] as string) != null) ? JsonConvert.DeserializeObject<Dictionary<string, FunctionVariable>>(reader["parameters"] as string) : null;
                                function.Inputs = ((reader["inputs"] as string) != null) ? JsonConvert.DeserializeObject<Dictionary<string, FunctionVariable>>(reader["inputs"] as string) : null;
                                function.Outputs = ((reader["outputs"] as string) != null) ? JsonConvert.DeserializeObject<Dictionary<string, FunctionVariable>>(reader["outputs"] as string) : null;
                            }

                            functions.Add(function);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to read functions");
            }

            return functions;
        }

        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Delete(long id)
        {
            var userId = GetUserId();
            if (!userId.HasValue) return Unauthorized();
            if (!hasRight(userId.Value, developerRight)) return Unauthorized();

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString))
                    {
                        connection.Open();
                        {
                            NpgsqlCommand command = new NpgsqlCommand("UPDATE versions SET modified_by = (SELECT users.username FROM users WHERE users.user_id = @user_id) WHERE function_id = @function_id", connection);
                            command.Parameters.AddWithValue("@user_id", userId);
                            command.Parameters.AddWithValue("@function_id", id);
                            command.ExecuteNonQuery();
                        }
                        {
                            NpgsqlCommand command = new NpgsqlCommand("DELETE FROM versions WHERE function_id = @function_id", connection);
                            command.Parameters.AddWithValue("@function_id", id);
                            command.ExecuteNonQuery();
                        }
                        {
                            NpgsqlCommand command = new NpgsqlCommand("UPDATE functions SET modified_by = (SELECT users.username FROM users WHERE users.user_id = @user_id) WHERE function_id = @function_id", connection);
                            command.Parameters.AddWithValue("@user_id", userId);
                            command.Parameters.AddWithValue("@function_id", id);
                            command.ExecuteNonQuery();
                        }
                        {
                            NpgsqlCommand command = new NpgsqlCommand("DELETE FROM functions WHERE function_id = @function_id", connection);
                            command.Parameters.AddWithValue("@function_id", id);
                            command.ExecuteNonQuery();
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to delete function");
            }

            return Json(GetFunctions(userId.Value));
        }

        private FunctionModel GetFunction(long id)
        {
            FunctionModel function = null;
            try
            {
                using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString))
                {
                    connection.Open();

                    {
                        NpgsqlCommand command = new NpgsqlCommand("SELECT functions.function_id, functions.description, functions.name, functions.format, functions.local, functions.parameters, functions.inputs, functions.outputs FROM functions WHERE functions.function_id = @function_id", connection);
                        command.Parameters.AddWithValue("@function_id", id);
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                function = new FunctionModel()
                                {
                                    Stored = true,
                                    Id = Convert.ToInt64(reader["function_id"]),
                                    Description = reader["description"] as string,
                                    Name = reader["name"] as string,
                                    Format = reader["format"] as string,
                                    Local = reader["local"] as bool?,
                                    Parameters = (reader["parameters"] != DBNull.Value) ? JsonConvert.DeserializeObject<Dictionary<string, FunctionVariable>>(reader["parameters"] as string) : new Dictionary<string, FunctionVariable>(),
                                    Inputs = (reader["inputs"] != DBNull.Value) ? JsonConvert.DeserializeObject<Dictionary<string, FunctionVariable>>(reader["inputs"] as string) : new Dictionary<string, FunctionVariable>(),
                                    Outputs = (reader["outputs"] != DBNull.Value) ? JsonConvert.DeserializeObject<Dictionary<string, FunctionVariable>>(reader["outputs"] as string) : new Dictionary<string, FunctionVariable>(),
                                };
                            }
                        }
                    }

                    if (function != null)
                    {
                        function.Versions = new List<Models.Version>();
                        NpgsqlCommand command = new NpgsqlCommand("SELECT version_id, number, description, version_state_id FROM versions WHERE function_id = @function_id", connection);
                        command.Parameters.AddWithValue("@function_id", id);
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                function.Versions.Add(new Models.Version()
                                {
                                    Stored = true,
                                    Id = Convert.ToInt64(reader["version_id"]),
                                    Number = reader["number"] as string,
                                    Description = reader["description"] as string,
                                    StateId = Convert.ToInt64(reader["version_state_id"]),
                                });
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to read function");
            }

            return function;
        }

        public IActionResult Detail(long id)
        {
            var userId = GetUserId();
            if (!userId.HasValue) return Unauthorized();

            return Json(GetFunction(id));
        }

        public IActionResult DownloadVersion(long id, long versionId)
        {
            var userId = GetUserId();
            if (!userId.HasValue) return Unauthorized();
            if (!hasRight(userId.Value, developerRight)) return Unauthorized();

            try
            {
                using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString))
                {
                    connection.Open();

                    {
                        NpgsqlCommand command = new NpgsqlCommand("SELECT code FROM versions WHERE function_id = @function_id AND version_id = @version_id", connection);
                        command.Parameters.AddWithValue("@function_id", id);
                        command.Parameters.AddWithValue("@version_id", versionId);
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                var fileName = string.Format("function_{0}_version_{1}.dat", id, versionId);
                                var bytes = reader["code"] as byte[];
                                return File(bytes, "application/octet-stream", fileName);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to read version file");
            }

            return new EmptyResult();
        }

        [HttpPost]
        public IActionResult Edit(string functionJson, List<IFormFile> files)
        {
            logger.LogInformation("Saving funtion");

            var userId = GetUserId();
            if (!userId.HasValue) return Unauthorized();
            if (!hasRight(userId.Value, developerRight)) return Unauthorized();

            var function = JsonConvert.DeserializeObject<FunctionModel>(functionJson);
            var listOfFunctions = new List<long>();

            try {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString))
                    {
                        connection.Open();

                        {
                            NpgsqlCommand command = null;
                            if (function.Stored)
                            {
                                command = new NpgsqlCommand("UPDATE functions SET description = @description, name = @name, format = @format, local = @local, parameters = @parameters, inputs = @inputs, outputs = @outputs, modified_by = (SELECT users.username FROM users WHERE users.user_id = @user_id) WHERE function_id = @function_id", connection);
                                command.Parameters.AddWithValue("@function_id", function.Id);                                
                            }
                            else
                            {
                                command = new NpgsqlCommand("INSERT INTO functions (description, name, format, local, parameters, inputs, outputs, modified_by) VALUES (@description, @name, @format, @local, @parameters, @inputs, @outputs, (SELECT users.username FROM users WHERE users.user_id = @user_id));select currval('function_function_id_seq');", connection);
                            }

                            command.Parameters.AddWithValue("@user_id", userId);
                            if (function.Description != null) command.Parameters.AddWithValue("@description", function.Description);
                            else command.Parameters.AddWithValue("@description", DBNull.Value);
                            if (function.Name != null) command.Parameters.AddWithValue("@name", Regex.Replace(function.Name, @"\W", "_"));
                            else command.Parameters.AddWithValue("@name", DBNull.Value);
                            if (function.Format != null) command.Parameters.AddWithValue("@format", function.Format);
                            else command.Parameters.AddWithValue("@format", DBNull.Value);
                            command.Parameters.AddWithValue("@local", function.Local ?? false);
                            if (function.Parameters != null && function.Parameters.Any()) command.Parameters.AddWithValue("@parameters", JsonConvert.SerializeObject(function.Parameters));
                            else command.Parameters.AddWithValue("@parameters", DBNull.Value);
                            if (function.Inputs != null && function.Inputs.Any()) command.Parameters.AddWithValue("@inputs", JsonConvert.SerializeObject(function.Inputs));
                            else command.Parameters.AddWithValue("@inputs", DBNull.Value);
                            if (function.Outputs != null && function.Outputs.Any()) command.Parameters.AddWithValue("@outputs", JsonConvert.SerializeObject(function.Outputs));
                            else command.Parameters.AddWithValue("@outputs", DBNull.Value);

                            if (function.Stored)
                            {
                                command.ExecuteNonQuery();
                            }
                            else
                            {
                                function.Id = Convert.ToInt64(command.ExecuteScalar());                                
                            }
                            if (!listOfFunctions.Contains(function.Id))
                                listOfFunctions.Add(function.Id);                            
                        }

                        int filesPosition = 0;
                        foreach (var version in function.Versions)
                        {
                            NpgsqlCommand command = null;
                            if (version.Stored)
                            {
                                command = new NpgsqlCommand("UPDATE versions SET description = @description, number = @number, version_state_id = @version_state_id, modified_by = (SELECT users.username FROM users WHERE users.user_id = @user_id) WHERE version_id = @version_id", connection);
                                command.Parameters.AddWithValue("@version_id", version.Id);
                            }
                            else
                            {
                                command = new NpgsqlCommand("INSERT INTO versions (function_id, description, number, version_state_id, code, modified_by) VALUES (@function_id, @description, @number, @version_state_id, @code, (SELECT users.username FROM users WHERE users.user_id = @user_id));select currval('version_version_id_seq');", connection);
                                command.Parameters.AddWithValue("@function_id", function.Id);
                                command.Parameters.AddWithValue("@code", GetFileBytes(files[filesPosition]));
                                filesPosition++;                                
                            }

                            command.Parameters.AddWithValue("@user_id", userId);
                            if (version.Description != null) command.Parameters.AddWithValue("@description", version.Description);
                            else command.Parameters.AddWithValue("@description", DBNull.Value);
                            if (version.Number != null) command.Parameters.AddWithValue("@number", version.Number);
                            else command.Parameters.AddWithValue("@number", DBNull.Value);
                            command.Parameters.AddWithValue("@version_state_id", version.StateId);

                            if (version.Stored)
                            {
                                command.ExecuteNonQuery();
                            }
                            else
                            {
                                version.Id = Convert.ToInt64(command.ExecuteScalar());
                            }                            
                        }
                        if (!listOfFunctions.Contains(function.Id))
                            listOfFunctions.Add(function.Id);
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to edit function");
            }

            foreach (var function_id in listOfFunctions) {
                Publish($"manager/functions/{function_id}", "updated");
            }
            return Json(GetFunction(function.Id));
        }

        private byte[] GetFileBytes(IFormFile file)
        {
            var buffer = new byte[1024 * 4];
            using (var writeStream = new MemoryStream())
            {
                using (var readStream = file.OpenReadStream())
                {
                    int count = 0;
                    do
                    {
                        count = readStream.Read(buffer, 0, buffer.Length);
                        writeStream.Write(buffer, 0, count);
                    } while (count > 0);
                }

                writeStream.Flush();
                writeStream.Seek(0,SeekOrigin.Begin);

                var result = new byte[file.Length];
                writeStream.Read(result, 0, result.Length);

                return result;
            }
        }
    }
}